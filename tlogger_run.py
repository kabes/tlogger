#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2020 Karol Będkowski <Karol Będkowski@kntbk>
#
# Distributed under terms of the GPLv3 license.

"""Tlogger start file."""

import os

try:
    import stackprinter

    stackprinter.set_excepthook(style="color")
except ImportError:
    try:
        from rich.traceback import install

        install()
    except ImportError:
        pass
try:
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

    import traceback

    def ic_stack(*args, **kwargs):
        ic("\n".join(traceback.format_stack()[:-2]), *args, **kwargs)

    import inspect

    class ShiftedIceCreamDebugger(icecream.IceCreamDebugger):
        def format(self, *args):
            # one more frame back
            call_frame = inspect.currentframe().f_back.f_back
            out = self._format(call_frame, *args)
            return out

    sic = ShiftedIceCreamDebugger()

    def ic_trace(func):
        def wrapper(*args, **kwargs):
            sic(func, args, kwargs)
            res = func(*args, **kwargs)
            sic(func, res)
            return res

        return wrapper

    import builtins

    setattr(builtins, "ic_stack", ic_stack)
    setattr(builtins, "ic_trace", ic_trace)

except ImportError:  # Graceful fallback if IceCream isn't installed.
    pass

# try:
#     from typeguard import install_import_hook

#     install_import_hook("tlogger")
#     print("WARN! typeguard hook installed")
# except ImportError as err:
#     print(err)

from tlogger.cli import cli

os.environ["EDITOR"] = "vim"

try:
    cli(None, None)
except (KeyboardInterrupt, EOFError):
    pass
