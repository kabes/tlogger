tlogger v3.1.x
==============

Console time logger.

Insipred by: timeflow [https://github.com/trimailov/timeflow] and gtimelog
[http://gtimelog.org/] (similar file format).

Task are logged in text filed in format: `<date>: <comment>`.

Date is in in format `yyyy-mm-dd hh:mm` (24-hours).

Comments my starts with "project name" (name without spaces ended with :).

If comment ends with `**` is treated as virtual task (not counted) - i.e.
start working.

Comments staring with `#` are notes records.
But if `#` is on other place - content after it is ignored.

Comments starting with `*` are counted events; default one record is counted
as one but this may be changes by adding number after `*`.


If comment contain `#` - all after this is note and is skipped in reports.

Example records::

  2018-01-01 12:23: start **
  2019-01-01 13:24: coffee: +1
  2018-01-01 13:00: finish task1
  2018-01-01 13:23: start **
  2018-01-01 13:24: # note about something
  2018-01-01 13:30: proj1: finish task2
  2019-01-01 13:32: coffee: +1 another coffe
  2018-01-01 13:50: proj1: finish task3 # done part 1
  2018-01-01 14:00: # another note


Alternativly records may be stored in sqlite3 database (when log file name
ends with `.db` or `.sqlite` ). Migration between text and db file is
available.



Running
-------
::

  tlogger3 [OPTIONS] COMMAND [ARGS]...

  Options:
    -c, --conf FILE                 Configuration file
    -f, --file FILE                 Main log file name
    --duration-format [human|time|minutes]
                                    Determine how to format duration
    --pager [auto|never|always]     Use pager for output
    -v, --verbose                   Verbose mode; use twice for debug
    --version                       Show the version and exit.
    -h, --help                      Show this message and exit.

  Commands:
    add           Add entry to log.
    archive       Move old (previous years) records into separate files.
    check         Check file for errors.
    counter       Add counter-like record to log.
    day           Edit current on given day records.
    edit          Edit log file with external editor.
    end           Log ended task with given duration.
    import        Import raw data into log file.
    migrate       Migrate data to other storage
    note          Add note to log.
    pomodoro      Pomodoro timer.
    print         Print records with or without formatting.
    print-config  Display current program configuration.
    report        Show records report.
    show          Show informations about records (projects, tags).
    start         Log "start**" record.
    status        Display information about today records.
    today         Show report for today records.
    undo          Remove last record from logfile.
    yesterday     Show report for yesterday records.


Help for commands::

   tlogger3 <command> --help


Example workflow
----------------

1. log start working::

    tlogger3 start

2. log finished "task 1" in "project-1"::

    tlogger3 add project-1: task 1


3. work on another task, and mark it finish::

    tlogger3 add task2

4. take a break

5. log start work again (manual method)::

    tlogger3 add start working after break**

6. finish "task 1" again::

    tlogger3 add project-1: task 1

7. get status or report for today work::

    tlogger3 status
    tlogger3 today
    tlogger3 report -g by-project -p today

8. add some note::

    tlogger3 note this is some note


9. count some events::

    tlogger3 counter project1:bugs_resolved: +3
    tlogger3 counter coffe: +2


10. show / edit current / previous day in editor::

     tlogger3 day
     tlogger3 day -1



Shell autocomplete
------------------

Bash::

   eval "$(_TLOGGER3_COMPLETE=bash_source tlogger3)"

Zsh::

   eval "$(_TLOGGER3_COMPLETE=zsh_source tlogger3)"


Fish::

   eval (env _TLOGGER3_COMPLETE=fish_source tlogger3)



Licence
-------

Copyright (c) Karol Będkowski, 2018-2023

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

For details please see COPYING file.
