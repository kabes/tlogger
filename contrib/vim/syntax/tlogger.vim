" Vim tlogger syntax file

if exists("b:current_syntax")
  finish
endif

let b:current_syntax = "tlogger"

syn clear
syn case match
syn match tloggerComment      /^#.\+$/
syn match tloggerNote         /\ #.\+$/
syn match tloggerInc          / +\d\+\(\.\d+\+\)\?\>/
syn match tloggerProject      / \([a-zA-Z\u0100-\uFFFF][a-zA-Z0-9\u0100-\uFFFF_]*:\)\+ /
syn match tloggerMarker       / \*\*/

hi def link tloggerComment	Comment
hi def link tloggerNote		String
hi def link tloggerInc		Number
hi def link tloggerProject	Title
hi def link tloggerMarker	Character

" vim: ts=4
