#! /usr/bin/env python
# vim:fenc=utf-8
#
# Copyright © 2020  Karol Będkowski
#
# Distributed under terms of the GPLv3 license.

"""Common formatting functions."""
from __future__ import annotations

import collections
import itertools
import math
import textwrap
import typing as ty
from datetime import date, datetime
from enum import IntEnum

TS_FORMAT = "%Y-%m-%d %H:%M"
TST_FORMAT = "%H:%M"
TSD_FORMAT = "%Y-%m-%d"
TS_STR_LEN = 16

# -----------------------------------------------------------
# formatting & display
# -----------------------------------------------------------

OptDateTimeStamp = ty.Union[datetime, date, None]
OptDuration = ty.Union[int, float, None]


def format_datetime(
    date_time: OptDateTimeStamp, with_date: bool = False
) -> str:
    if not date_time:
        return ""

    if with_date:
        return date_time.strftime(TS_FORMAT)

    return date_time.strftime(TST_FORMAT)


def format_date(date_time: OptDateTimeStamp) -> str:
    if not date_time:
        return ""

    return date_time.strftime(TSD_FORMAT)


def format_duration(minutes: OptDuration = 0, sec: int = 0) -> str:
    total = (minutes or 0) + math.ceil(sec / 60)
    if not total:
        return ""

    ihours = int(total // 60)
    iminutes = int(total % 60)

    match (ihours, iminutes):
        case (0, minutes):
            return f"{minutes}m"
        case (hours, 0):
            return f"{hours}h"

    return f"{ihours}h {iminutes}m"


def format_duration_min(minutes: OptDuration = 0, sec: int = 0) -> str:
    total = (minutes or 0) + math.ceil(sec / 60)
    return str(total)


def format_duration_ts(minutes: OptDuration = 0, sec: int = 0) -> str:
    total = (minutes or 0) + math.ceil(sec / 60)
    ihours = int(total // 60)
    iminutes = int(total % 60)
    return f"{ihours:0d}:{iminutes:02d}"


class DurationFormatter(ty.Protocol):  # pylint: disable=too-few-public-methods
    """
    DurationFormatter define function type used to format duration into string.
    """

    def __call__(self, minutes: OptDuration = None, sec: int = 0) -> str:
        ...


def get_duration_formatter(name: str | None) -> DurationFormatter:
    """
    Get function that format duration to str.
    """
    if name == "minutes":
        return format_duration_min

    if name == "time":
        return format_duration_ts

    return format_duration


class ColAlign(IntEnum):
    LEFT = 0
    CENTER = 1
    RIGHT = 2

    def format(self, text: str | None, dst_width: int) -> str:
        if not text:
            return " " * dst_width

        missing_spaces = dst_width - len(text)
        if missing_spaces <= 0:
            return text

        match self.value:
            case ColAlign.CENTER:
                text = " " * (missing_spaces // 2) + text
                return text + " " * (dst_width - len(text))

            case ColAlign.RIGHT:
                return " " * missing_spaces + text

        return text + " " * missing_spaces


class Formatter(ty.Protocol):  # pylint: disable=too-few-public-methods
    def __call__(
        self,
        rows: ty.Iterable[ty.Iterable[ty.Any]],
        header: ty.Sequence[str] | None = None,
        align: ty.Sequence[ColAlign] | None = None,
        opts: dict[str, ty.Any] | None = None,
    ) -> ty.Iterable[str]:
        pass


def _calc_col_width(
    rows: ty.Iterable[ty.Iterable[ty.Any]],
    header: ty.Sequence[str] | None = None,
    opts: dict[str, ty.Any] | None = None,
) -> dict[int, int]:
    cols_width: dict[int, int] = collections.defaultdict(int)

    if header:
        cols_width.update(
            (col_idx, len(head)) for col_idx, head in enumerate(header)
        )

    for row in rows:
        for col_idx, col in enumerate(row):
            cols_width[col_idx] = max(cols_width[col_idx], len(str(col)))

    if not opts:
        return cols_width

    wrapped_col = opts.get("col-wrap")
    if wrapped_col is None:
        return cols_width

    if wrapped_col < 0:
        wrapped_col = len(cols_width) + wrapped_col

    max_width = opts.get("max-width")
    total_len = sum(cols_width.values()) + 4 * len(cols_width)
    if max_width and max_width < total_len:
        # try reduce column to wrap to match max len
        c_size = max_width - (total_len - cols_width[wrapped_col])
        if c_size > 1:
            cols_width[wrapped_col] = c_size

    return cols_width


def tabularize(
    rows: ty.Iterable[ty.Iterable[ty.Any]],
    header: ty.Sequence[str] | None = None,
    align: ty.Sequence[ColAlign] | None = None,
    opts: dict[str, ty.Any] | None = None,
) -> ty.Iterable[str]:
    """
    Arrange items into tabular format.

    Parameters:
        rows: data
        header: optional header
        align: allow to align each column on left (0), center(1) or right (2);
            default is align to left
    """
    rows0, rows1 = itertools.tee(rows)
    cols_width = _calc_col_width(rows0, header, opts)

    if miss_cols := len(cols_width) - len(align or ()):
        align = list(align or [])
        align.extend([ColAlign.LEFT] * miss_cols)

    if header:
        header_row = "    ".join(
            item + " " * (cols_width[idx] - len(item))
            for idx, item in enumerate(map(str, header))
        )
        yield header_row.strip()
        yield "-" * len(header_row)

    def wrap_cells(idx: int, value: ty.Any) -> list[str]:
        val = str(value)
        if (col_width := cols_width[idx]) < len(val):
            return textwrap.wrap(val, col_width)

        return [val]

    def format_cell(idx: int, value: str) -> str:
        assert align
        col_width = cols_width[idx]
        col_align = align[idx]
        return col_align.format(value, col_width)

    for row in rows1:
        rows_wrapped = itertools.starmap(wrap_cells, enumerate(row))
        for subrow in itertools.zip_longest(*rows_wrapped):
            formatted_row = "    ".join(
                itertools.starmap(format_cell, enumerate(subrow))
            )
            yield formatted_row.rstrip()


def _escape_cell(cell: ty.Any) -> str:
    cells = str(cell).replace('"', '""')
    if "," in cells or "\n" in cells or "\r" in cells:
        return f'"{cells}"'

    return cells


def format_csv(
    rows: ty.Iterable[ty.Iterable[ty.Any]],
    header: ty.Sequence[str] | None = None,
    align: ty.Sequence[ColAlign] | None = None,
    opts: dict[str, ty.Any] | None = None,
) -> ty.Iterable[str]:
    """
    Basic cvs implementation
    """

    if header:
        yield ",".join(map(_escape_cell, header))

    for row in rows:
        yield ",".join(map(_escape_cell, row))


def get_report_formatter(name: str | None) -> Formatter:
    """
    Ger report formatter according to `name`.
    """
    if name == "csv":
        return format_csv

    return tabularize
