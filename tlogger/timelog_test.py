#! /usr/bin/env python
# vim:fenc=utf-8
#
# Copyright © 2020 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.
# pylint: disable=protected-access,unspecified-encoding,consider-using-with

"""Timelog tests."""

import datetime
import os.path
import tempfile
import unittest
from contextlib import suppress

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)


from . import timelog
from .config import Conf, ConfKey

_T1_CONTENT = """
2019-06-19 15:13: start **
#2019-06-19 15:16: learn: r
2019-06-19 19:38: learn r2
2019-06-19 19:42: start **
2019-06-19 20:04: tlogger: cleanup *
2019-06-19 20:16: learn r2

2019-06-20 13:03: start **
2019-06-20 13:26: tlogger: cleanup
2019-06-20 13:37: tlogger: cleanup
2019-06-20 14:07: start **
"""

_T2_CONTENT = ["2019-06-19 15:13: start **", "2019-06-19 19:38: learn r2"]

_T3_CONTENT = [
    "2019-06-19 15:13: start **",
    "# comments",
    "2019-06-19 19:38: learn r2",
    "",
    "2019-06-19 19:42: start **",
    " ",
]


class TestFileOperation(unittest.TestCase):
    def __init__(self, *args, **kwarg):
        super().__init__(*args, **kwarg)
        self.fpath = None
        self.tmpdir = None

    def setUp(self):
        self.tmpdir = tempfile.TemporaryDirectory()

    def tearDown(self):
        self.tmpdir.cleanup()

    def _create_sample(self):
        self.fpath = fpath = os.path.join(self.tmpdir.name, "tlogger.txt")
        with open(fpath, "w", encoding="UTF-8") as fout:
            fout.write(_T1_CONTENT)

    def test_load_file(self):
        self._create_sample()
        conf = Conf({ConfKey.MAIN_LOG_FILE: self.fpath})
        tlog = timelog.TimeLog(conf)
        records = list(tlog.load())
        self.assertEqual(len(records), 9)

    def test_append(self):
        self._create_sample()
        conf = Conf({ConfKey.MAIN_LOG_FILE: self.fpath})
        tlog = timelog.TimeLog(conf)
        records = list(tlog.load())
        print(tlog.append("2020-01-01 15:00: test"))

        tlog = timelog.TimeLog(conf)
        records = list(tlog.load())
        self.assertEqual(len(records), 10)
        self.assertEqual(records[-1].human_timestamp(), "2020-01-01 15:00")
        self.assertEqual(records[-1].raw, "2020-01-01 15:00: test")

        print(tlog.append("2020-01-01 16:00: test2"))

        tlog = timelog.TimeLog(conf)
        records = list(tlog.load())
        self.assertEqual(len(records), 11)
        self.assertEqual(records[-1].human_timestamp(), "2020-01-01 16:00")
        self.assertEqual(records[-1].raw, "2020-01-01 16:00: test2")

        with open(self.fpath) as fin:
            content = fin.readlines()
            self.assertEqual(content[-3].strip(), "")
            self.assertEqual(content[-2].strip(), "2020-01-01 15:00: test")
            self.assertEqual(content[-1].strip(), "2020-01-01 16:00: test2")

    def test_last_record(self):
        self._create_sample()
        conf = Conf({ConfKey.MAIN_LOG_FILE: self.fpath})
        tlog = timelog.TimeLog(conf)
        last = tlog.get_last_record()[0]
        self.assertEqual(last.raw, "2019-06-20 14:07: start **")

        tlog.append("2020-01-01 15:30: test")

        last = tlog.get_last_record()[0]
        self.assertEqual(last.raw, "2020-01-01 15:30: test")

    def test_last_undo(self):
        self._create_sample()
        conf = Conf({ConfKey.MAIN_LOG_FILE: self.fpath})
        tlog = timelog.TimeLog(conf)
        tlog.undo()

        with open(self.fpath) as fin:
            content = fin.readlines()
            self.assertEqual(len(content), 11)
            self.assertEqual(
                content[-1].strip(), "2019-06-20 13:37: tlogger: cleanup"
            )

        tlog.undo()
        tlog.undo()
        tlog.undo()
        tlog.undo()

        with open(self.fpath) as fin:
            content = fin.readlines()
            self.assertEqual(len(content), 6)
            self.assertEqual(
                content[-1].strip(), "2019-06-19 20:04: tlogger: cleanup *"
            )


class TestPrepare(unittest.TestCase):
    def test_split_simple(self):
        line = "abcd abnd: dlksd lksdl"
        self.assertEqual(
            timelog._prepare_split_parts(line), (None, None, line)
        )

        line = "abcd abnd123:dlk sd: lksdl"
        self.assertEqual(
            timelog._prepare_split_parts(line), (None, None, line)
        )

        line = "12:12 abcd abnd123:dlk sd: lksdl"
        self.assertEqual(
            timelog._prepare_split_parts(line), (None, None, line)
        )

        line = "2021-12-31 12:23 abcd abnd123:dlk sd: lksdl"
        self.assertEqual(
            timelog._prepare_split_parts(line), (None, None, line)
        )

    def test_split_date(self):
        line = "2021-01-02: abcd"
        self.assertEqual(
            timelog._prepare_split_parts(line), ("2021-01-02", None, "abcd")
        )

        line = "2021-01-02 01:23: abcd"
        self.assertEqual(
            timelog._prepare_split_parts(line),
            ("2021-01-02 01:23", None, "abcd"),
        )

        line = "2021-01-02 01:23:45: 2021-01-01: abcd"
        self.assertEqual(
            timelog._prepare_split_parts(line),
            ("2021-01-02 01:23:45", None, "2021-01-01: abcd"),
        )

    def test_split_project(self):
        line = "2021-01-02: abcd: test"
        self.assertEqual(
            timelog._prepare_split_parts(line), ("2021-01-02", "abcd", "test")
        )

        line = "2021-01-02 01:23: abcd:bcd: abc"
        self.assertEqual(
            timelog._prepare_split_parts(line),
            ("2021-01-02 01:23", "abcd:bcd", "abc"),
        )

        line = "abc:efg: abcd"
        self.assertEqual(
            timelog._prepare_split_parts(line),
            (None, "abc:efg", "abcd"),
        )

    def test_prepare(self):
        dts = datetime.date(2021, 1, 2)
        line = " abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)), [line]
        )

        line = "\x09abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)), [line]
        )

    def test_prepare_date(self):
        dts = datetime.date(2021, 1, 2)
        line = "**: abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2021-01-02 00:00: abc"],
        )

        line = "*: abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2021-01-02: abc"],
        )

        line = "abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2021-01-02: abc"],
        )

        line = "2022-02-03: abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2022-02-03: abc"],
        )

        line = "2022-02-03: aaa: abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2022-02-03: aaa: abc"],
        )

        line = "aaa: abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2021-01-02: aaa: abc"],
        )

    def test_prepare_datetime(self):
        dts = datetime.datetime(2021, 1, 2, 12, 34, 56)
        line = "**: abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2021-01-02 12:34: abc"],
        )

        line = "*: abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2021-01-02: abc"],
        )

        line = "abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2021-01-02 12:34: abc"],
        )

        line = "2022-02-03: abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2022-02-03: abc"],
        )

        line = "2022-02-03 11:23: aaa: abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2022-02-03 11:23: aaa: abc"],
        )

        line = "aaa: abc"
        self.assertEqual(
            list(timelog.prepare_records_from_str(line, dts)),
            ["2021-01-02 12:34: aaa: abc"],
        )
