#! /usr/bin/env python3
# vim:fenc=utf-8
#
# Copyright © 2022 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
sqlite store

Schema:
    ts_day: date; = record.day; = record.end if daily record
    ts_begin: date + time; if daily record -> null, other begin
    ts_end: date + time; if daily record -> null, otherwise = end
    kind: = record kind
    content: message + optional inline comment
    project: record project
    note: additional note

    record raw = (ts or ts_day) + project + content

"""
from __future__ import annotations

import datetime
import logging
import sqlite3
import time
import typing as ty
from contextlib import suppress
from decimal import Decimal

from . import filters, utils
from .config import Conf, ConfKey
from .model import Context, RecordType
from .record import Record, Records, from_string
from .store import AbstractStore

_LOG = logging.getLogger(__name__)


def adapt_decimal(value: Decimal | None) -> bytes:
    if value is None:
        return b""

    if value != value.to_integral():
        return str(value.quantize(Decimal(".00000001"))).encode("ascii")

    return str(value).encode("ascii")


def convert_decimal(value: bytes | None) -> Decimal | None:
    if not value:
        return None

    return Decimal(value.decode("ascii"))


sqlite3.register_adapter(Decimal, adapt_decimal)
sqlite3.register_converter("decimal", convert_decimal)


class SqliteStore(AbstractStore):
    """
    SqliteStore store records in plain file.

    """

    def __init__(self, cfg: Conf):
        self.fname = cfg[ConfKey.MAIN_LOG_FILE]
        self._con: sqlite3.Connection | None = None

    def open(self) -> None:
        """
        Open store, lock it.
        """
        # pylint: disable=consider-using-with
        _LOG.debug("opening `%s`", self.fname)
        self._con = sqlite3.connect(
            self.fname, detect_types=sqlite3.PARSE_DECLTYPES
        )
        self._con.row_factory = sqlite3.Row
        self._check_schema()

    def close(self) -> None:
        """
        Close store, unlock it.
        """
        if self._con:
            self._con.close()

    # pylint: disable=R0801
    def load(
        self,
        ctx: Context,
        fltr: filters.Filter | None = None,
    ) -> Records:
        """
        Load records from database.

        Args:
            ctx: context
            fltr: optional filters

        Returns:
            Records
        """
        assert self._con

        where, args = filter_to_sql(fltr)
        where_sql = " where " + " and ".join(where) if where else ""
        sql = f"select * from records {where_sql} order by ts_day, ts_end, id"

        _LOG.debug("load sql: `%s`, '%r'", sql, args)

        cur = self._con.cursor()
        cur.execute(sql, args)
        records: Records = map(_row2record, cur)
        if fltr:
            records = fltr.filter_items(records)

        yield from records

    def get_last_record(
        self,
    ) -> tuple[Record | None, Record | None]:
        """
        Get fast last record from current log file not older than 24h.
        Do not update records.

        Returns:
            (last record in file, oldest record in file)
        """
        assert self._con

        last_record: Record | None = None
        oldest_record: Record | None = None

        today = datetime.date.today()

        cur = self._con.cursor()
        cur.execute(
            "select * from records where ts_day >= ? order by id desc limit 1",
            (today,),
        )
        if row := cur.fetchone():
            last_record = _row2record(row)

        cur.execute(
            "select * from records where ts_end is not null and ts_day >= ? "
            "order by ts_end desc limit 1",
            (today,),
        )
        if row := cur.fetchone():
            oldest_record = _row2record(row)
        else:
            oldest_record = last_record

        return last_record, oldest_record

    def get_last_records_msg(
        self, num: int, *kind: RecordType
    ) -> ty.Iterable[str]:
        """
        Get unique last `num` records message given type.
        Read only current and previous year files (if exists).

        Args:
            num: number of records to load
            kind: list types of records to load

        Returns:
            last messages as str
        """
        assert self._con

        kind = kind or (RecordType.RECORD,)

        sql = """
WITH
mixed AS (
    SELECT id,
        TRIM(CASE WHEN project != '' THEN project || ': ' || content
            ELSE content END) AS content
    FROM records
    WHERE kind IN (##)
),
d AS (
    SELECT content, MAX(id) OVER (PARTITION BY content) AS r
    FROM mixed
)
SELECT DISTINCT content
FROM d
ORDER BY r DESC
LIMIT
"""
        args = [k.value for k in kind]
        sql = sql.replace("##", ",".join(["?"] * len(args)))
        sql += str(num)  # add limit
        cur = self._con.cursor()
        cur.execute(sql, args)
        for row in cur:
            yield row[0]

    def append(self, records: Records) -> tuple[int, int]:
        """
        Appends records into store.

        Args:
            records: records to add

        Returns:
            (number of added records, optional duration from previous record)
            Where number added records exceed 10, add `...` and number of total
            records.
        """
        assert self._con

        _, oldest_record = self.get_last_record()
        head, records = utils.peek(records)
        if not head:
            return 0, 0

        days_to_reindex: set[datetime.date] = set()
        cur = self._con.cursor()

        def prepare(rec: Record) -> tuple[ty.Any, ...]:
            assert rec.day
            days_to_reindex.add(rec.day)
            return (
                rec.day,
                rec.begin,
                None if rec.daily_record else rec.end,
                rec.kind.value,
                rec.project,
                rec.cnt,
                rec.message,
                "\n".join(rec.note) if rec.note else None,
            )

        _LOG.debug("appending data...")

        cur.executemany(
            "insert into records "
            "(ts_day, ts_begin, ts_end, kind, project, cnt, content, note) "
            "values (?, ?, ?, ?, ?, ?, ?, ?)",
            map(prepare, records),
        )

        num_records = cur.rowcount

        _LOG.debug("reindex %d days...", len(days_to_reindex))
        self._reindex_days(days_to_reindex)
        self._con.commit()

        if self._con.total_changes > 500:
            _LOG.debug("optimize sqlite database..")
            self._con.executescript(
                "PRAGMA analysis_limit=1000; VACUUM; PRAGMA optimize;"
            )

        _LOG.debug("all done")

        first_record = head[0]
        if oldest_record:
            duration = (first_record.end - oldest_record.end).total_seconds()
            return num_records, int(duration)

        return num_records, 0

    def list(self) -> ty.Iterable[str]:
        """
        Get content of current file (year).
        """
        ctx = Context()
        fltr = filters.Filter(
            date_begin=datetime.date.today().replace(day=1, month=1)
        )
        for rec in self.load(ctx, fltr=fltr):
            yield rec.build_raw()

    def list_all(self) -> ty.Iterable[str]:
        """
        Get all records
        """
        ctx = Context()
        for rec in self.load(ctx):
            yield rec.build_raw()

    def get_day_records(self, date: datetime.date | None = None) -> Records:
        """
        Get records from current or given day.

        Args:
            data: optional date of records to fetch (default today)
        """
        date = date or datetime.date.today()
        fltr = filters.Filter(date_begin=date, date_end=date)
        return self.load(Context(), fltr=fltr)

    def undo(self) -> str:
        """
        Remove last record from file.

        Return:
            removed record.
        """
        assert self._con
        cur = self._con.cursor()
        cur.execute("select * from records order by 1 desc limit 1")
        row = cur.fetchone()
        if not row:
            return "<none>"

        rowid = row["id"]
        content = row["content"]
        _LOG.debug("removing row id %d", rowid)
        cur.execute("delete from records where id=?", (rowid,))

        return str(content)

    def update_day(
        self, date: datetime.date, content: ty.Iterable[str], replace: bool
    ) -> None:
        """
        Replace entries from given day by given.
        """
        assert self._con

        if not content:
            return

        day = time.mktime(date.timetuple())

        cur = self._con.cursor()
        if replace:
            cur.execute("delete from records where ts_day=?", (day,))

        records = from_string(Context(), content, True)
        self.append(records)

    def get_projects(
        self,
        ctx: Context,
        ts_begin: datetime.date | None = None,
        ts_end: datetime.date | None = None,
    ) -> ty.Iterable[str]:
        assert self._con
        where: list[str] = ["project is not null"]
        args: list[ty.Any] = []

        if ts_begin:
            where.append("ts_day >= ?")
            args.append(ts_begin)

        if ts_end:
            where.append("ts_day <= ?")
            args.append(ts_end)

        where_sql = " and ".join(where)
        sql = (
            f"select distinct project from records where {where_sql} "
            "order by 1"
        )

        cur = self._con.cursor()
        cur.execute(sql, args)
        return (row[0] for row in cur)

    def archive(self) -> ty.Generator[str, None, None]:
        """
        Archive old records.
        """
        assert self._con
        cur = self._con.cursor()

        yield "Vacuum.."
        cur.execute("VACUUM")
        yield from map(str, cur)

        yield "Optimize..."
        cur.execute("PRAGMA analysis_limit=1000")
        cur.execute("PRAGMA optimize")
        yield from map(str, cur)

        self._con.commit()
        yield "Done"

    def check(self) -> ty.Iterable[str]:
        """
        Check storage.
        """
        assert self._con
        cur = self._con.cursor()
        cur.execute("PRAGMA integrity_check")
        for res in cur:
            if res[0] == "ok":
                return

            yield str(res[0])

        self._con.commit()

    def _check_schema(self) -> None:
        assert self._con
        ver = self._get_schema_version()
        _LOG.debug("current schema ver %d", ver)
        cur = self._con.cursor()
        for idx, sql in enumerate(_SCHEMA_VERS[ver:], ver + 1):
            _LOG.debug("update schema ver %d", idx)
            cur.executescript(sql)
            cur.execute("insert into schema_version (ver) values (?)", (idx,))
            self._con.commit()

    def _get_schema_version(self) -> int:
        assert self._con
        with suppress(sqlite3.OperationalError):
            cur = self._con.cursor()
            cur.execute("select max(ver) from schema_version")
            return int(cur.fetchone()[0])

        return 0

    def _reindex_days(self, days: ty.Iterable[datetime.date]) -> None:
        assert self._con
        cur = self._con.cursor()
        cur.executemany(_REINDEX_DAYS_SQL, [[day] for day in days])


_REINDEX_DAYS_SQL = """
UPDATE records
SET ts_begin = d.prev_ts
FROM (
        SELECT r.id, lag(ts_end) OVER win AS prev_ts
        FROM records r
        WHERE ts_day  = ? AND ts_end IS NOT NULL
            AND kind IN ('marker', 'record')
        WINDOW win AS (ORDER BY ts_end)
) AS d
WHERE records.id = d.id
"""


def _row2record(row: sqlite3.Row) -> Record:
    note = row["note"]
    rec = Record(
        row["ts_end"]
        or datetime.datetime.combine(
            row["ts_day"], datetime.datetime.min.time()
        ),
        None,
        [note] if note else None,
    )
    rec.kind = RecordType(row["kind"])
    rec.project = row["project"]
    rec.daily_record = not row["ts_end"]
    rec.cnt = row["cnt"]
    rec.message = row["content"]
    rec.begin = row["ts_begin"]
    return rec


def filter_to_sql(
    fltr: filters.Filter | None,
) -> tuple[list[str], list[ty.Any]]:
    if not fltr:
        return [], []

    where: list[str] = []
    args: list[ty.Any] = []

    if drange := fltr.real_date_ranges():
        dmin, dmax = drange
        if dmin:
            where.append("ts_day >= ?")
            args.append(dmin)

        if dmax:
            where.append("ts_day <= ?")
            args.append(dmax)

        fltr.period = fltr.date_begin = fltr.date_end = None

    if projects := fltr.projects:
        pos, neg = filters.split_pos_neg(projects)
        sql, pargs = _parse_args("project", pos)
        if sql:
            where.append(sql)
            args.extend(pargs)

        sql, pargs = _parse_args("project", neg)
        if sql:
            where.append("not " + sql)
            args.extend(pargs)

        fltr.projects = None

    if words := fltr.words:
        pos, neg = filters.split_pos_neg(words)
        if pos:
            where.extend(["content like ?"] * len(pos))
            args.extend(f"%{word}%" for word in pos)

        if neg:
            where.extend(["content not like ?"] * len(neg))
            args.extend(f"%{word}%" for word in neg)

        fltr.words = None

    if fltr.types and "all" not in fltr.types:
        plc = ",".join(["?"] * len(fltr.types))
        where.append(f"kind in ({plc})")
        args.extend(fltr.types)
        fltr.types = None

    return where, args


def _parse_args(col: str, vals: ty.Iterable[str]) -> tuple[str, list[str]]:
    if not vals:
        return "", []

    plain_args: list[str] = []
    like_args = []
    for val in vals:
        if val.endswith("*"):
            like_args.append(val.rstrip("*") + "%")
        else:
            plain_args.append(val)

    where = []
    args = []
    if plain_args:
        where.append(f"{col} in (" + ",".join(["?"] * len(plain_args)) + ")")
        args.extend(plain_args)

    if like_args:
        where.extend([f"{col} like ?"] * len(like_args))
        args.extend(like_args)

    return "(" + " or ".join(where) + ")", args


_SCHEMA_VERS = [
    """
create table schema_version (ver integer);
create table records (
    id integer primary key autoincrement,
    ts_day integer not null,
    ts_begin integer,
    ts_end integer,
    kind varchar(20),
    project text,
    cnt decimal,
    content text,
    note text
);

create index records_ts_day_idx on records(ts_day);
create index records_kind_idx on records(kind);
create index records_project_idx on records(project);
    """,
    # schema v2 use normal date/timestamp instead of unix timestamp
    # migrate old data
    """
create table records_new (
    id integer primary key autoincrement,
    ts_day date not null,
    ts_begin timestamp,
    ts_end timestamp,
    kind varchar(20),
    project text,
    cnt decimal,
    content text,
    note text
);

insert into records_new
select null,
    date(ts_day, 'unixepoch', 'localtime'),
    CASE when ts_begin is null then null
        else datetime(ts_begin, 'unixepoch', 'localtime')
    end,
    CASE when ts_end is null then null
        else datetime(ts_end, 'unixepoch', 'localtime')
    end,
    kind, project, cnt, content, note
from records;

alter table records rename to records_old;

drop index records_ts_day_idx;
drop index records_kind_idx;
drop index records_project_idx;

alter table records_new rename to records;

create index records_ts_day_idx on records(ts_day);
create index records_kind_idx on records(kind);
create index records_project_idx on records(project);
    """,
]
