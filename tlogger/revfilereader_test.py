#! /usr/bin/env python
# vim:fenc=utf-8
#
# Copyright © 2020 Karol Będkowski <Karol Będkowski@kntbk>
#
# Distributed under terms of the GPLv3 license.
# pylint: disable=protected-access

"""Filters tests."""

import os
import tempfile
import unittest
from contextlib import suppress

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)


from .revfilereader import ReverseFileReader


class TestFilters(unittest.TestCase):
    def __init__(self, *args, **kwarg):
        super().__init__(*args, **kwarg)
        self.fpath = None
        self.tmpdir = None

    def setUp(self):
        self.tmpdir = tempfile.TemporaryDirectory()

    def tearDown(self):
        self.tmpdir.cleanup()

    def _create_sample(self, content):
        self.fpath = fpath = os.path.join(self.tmpdir.name, "input.txt")
        with open(fpath, "w", encoding="UTF-8") as fout:
            fout.write(content)

    def test_filter_by_words(self):
        sample = ["x" * (33 + x % 25) for x in range(1000)]
        self._create_sample("\n".join(sample))

        sample.reverse()

        with ReverseFileReader(self.fpath) as rfr:
            for idx, line in enumerate(rfr):
                self.assertEqual(line.strip(), sample[idx])
