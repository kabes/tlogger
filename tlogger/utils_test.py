#! /usr/bin/env python
# vim:fenc=utf-8
#
# Copyright © 2020 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.
# pylint: disable=protected-access,unspecified-encoding,consider-using-with

"""Timelog tests."""

import typing as ty
import unittest
from contextlib import suppress

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)


from . import utils


class TestPeek(unittest.TestCase):
    def test_peek1(self) -> None:
        inp: list[int] = []
        head, ite = utils.peek(inp)
        self.assertEqual(len(head), 0)
        self.assertEqual(len(list(ite)), 0)

        inp = [1]
        head, ite = utils.peek(inp)
        self.assertEqual(head, [1])
        self.assertEqual(list(ite), [1])

        inp = [1, 2, 3]
        head, ite = utils.peek(inp)
        self.assertEqual(head, [1])
        self.assertEqual(list(ite), [1, 2, 3])

    def test_peek2(self) -> None:
        inp: list[int] = []
        head, ite = utils.peek(inp, 2)
        self.assertEqual(len(head), 0)
        self.assertEqual(len(list(ite)), 0)

        inp = [1]
        head, ite = utils.peek(inp, 2)
        self.assertEqual(head, [1])
        self.assertEqual(list(ite), [1])

        inp = [1, 2, 3]
        head, ite = utils.peek(inp, 2)
        self.assertEqual(head, [1, 2])
        self.assertEqual(list(ite), [1, 2, 3])

        inp = list(range(100))
        head, ite = utils.peek(inp, 2)
        self.assertEqual(head, [0, 1])
        self.assertEqual(list(ite), inp)

    def test_peek3(self) -> None:
        inp: ty.Iterator[int] = (a for a in [])  # noqa: FURB109
        head, ite = utils.peek(inp, 2)
        self.assertEqual(len(head), 0)
        self.assertEqual(len(list(ite)), 0)

        inp = (a for a in (1,))
        head, ite = utils.peek(inp, 2)
        self.assertEqual(head, [1])
        self.assertEqual(list(ite), [1])

        inp = (a for a in (1, 2, 3))
        head, ite = utils.peek(inp, 2)
        self.assertEqual(head, [1, 2])
        self.assertEqual(list(ite), [1, 2, 3])

        inp = (a for a in range(100))
        head, ite = utils.peek(inp, 2)
        self.assertEqual(head, [0, 1])
        self.assertEqual(list(ite), list(range(100)))


class TestEveryN(unittest.TestCase):
    def test_evetyn1(self) -> None:
        totalcnt = [0]

        def callback(value: int) -> None:
            totalcnt[0] = value

        items = "abcdefghijkml"

        cen = utils.call_every_n(items, 4, callback)
        val = next(cen)
        self.assertEqual(val, "a")
        self.assertEqual(totalcnt, [0])
        val = next(cen)
        self.assertEqual(val, "b")
        self.assertEqual(totalcnt, [0])
        val = next(cen)
        self.assertEqual(val, "c")
        self.assertEqual(totalcnt, [0])
        val = next(cen)
        self.assertEqual(val, "d")
        self.assertEqual(totalcnt, [0])
        val = next(cen)
        self.assertEqual(val, "e")
        self.assertEqual(totalcnt, [4])
        val = next(cen)
        self.assertEqual(val, "f")
        self.assertEqual(totalcnt, [4])
        val = next(cen)
        self.assertEqual(val, "g")
        self.assertEqual(totalcnt, [4])
        val = next(cen)
        self.assertEqual(val, "h")
        self.assertEqual(totalcnt, [4])
        val = next(cen)
        self.assertEqual(val, "i")
        self.assertEqual(totalcnt, [8])
