#! /usr/bin/env python
# vim:fenc=utf-8
#
# Copyright © 2020 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.

"""TimeLog file format support."""
from __future__ import annotations

import datetime
import itertools
import logging
import re
import types
import typing as ty

from . import filters, formatters as fmt, parse, store_file, store_sqlite
from .config import Conf, ConfKey
from .model import Context, RecordType
from .record import Record, Records, from_string
from .store import AbstractStore

OptStr = ty.Optional[str]

_LOG = logging.getLogger("timelog")


def select_store(fname: str, cfg: Conf) -> AbstractStore:
    if fname.endswith((".db", ".sqlite")):
        _LOG.debug("selected sqlite store `%s`", fname)
        return store_sqlite.SqliteStore(cfg)

    _LOG.debug("selected plain text store `%s`", fname)
    return store_file.PlainFileStore(cfg)


class TimeLog:
    """Timelog-like file format support."""

    def __init__(
        self,
        cfg: Conf,
    ):
        self.cfg = cfg
        self.fname = cfg[ConfKey.MAIN_LOG_FILE]
        self.store: AbstractStore = select_store(self.fname, cfg)

    def __enter__(self) -> TimeLog:
        # pylint: disable=consider-using-with
        self.store.open()
        return self

    def __exit__(
        self,
        exc_type: ty.Type[BaseException] | None,
        exc: BaseException | None,
        traceback: types.TracebackType | None,
    ) -> bool | None:
        self.store.close()
        return exc_type is None

    def load(
        self,
        fltr: filters.Filter | None = None,
    ) -> Records:
        """Load records from store."""
        ctx = Context()
        yield from self.store.load(ctx, fltr=fltr)

    def append(
        self,
        message: str | ty.Iterable[str],
    ) -> tuple[Records, int, int]:
        """
        Append new message to current file with timestamp or now.
        Multi-line input is split into separated messages.

        Args:
            message: message to add

        Returns:
            (
                added line,
                duration from last entry (0 for no duration) in sec,
                number of records
            )
        """
        if not message:
            raise ValueError()

        messages: ty.Iterable[str] = (
            message.split("\n") if isinstance(message, str) else message
        )
        records = from_string(Context(), messages)
        records1, records2 = itertools.tee(records)
        num_rec, dur = self.store.append(records2)
        return records1, dur, num_rec

    def get_last_record(
        self,
    ) -> tuple[Record | None, Record | None]:
        """Get last today's records.

        Returns:
            (last record in file, oldest record in file)

        """
        return self.store.get_last_record()

    def get_last_records_msg(
        self, num: int, *kind: RecordType
    ) -> ty.Iterable[str]:
        """
        Get unique last `num` records message given type.
        Read only current and previous year files (if exists).
        """
        yield from self.store.get_last_records_msg(num, *kind)

    def list(self) -> ty.Iterable[str]:
        yield from self.store.list()

    def get_day_records(self, date: datetime.date | None = None) -> Records:
        return self.store.get_day_records(date)

    def archive(self) -> ty.Generator[str, None, None]:
        yield from self.store.archive()

    def undo(self) -> str:
        """Remove last entered record."""
        return self.store.undo()

    def update_day(
        self, date: datetime.date, content: ty.Iterable[str], replace: bool
    ) -> None:
        lines = [line for line in content if line and line[0] != "#"]
        if not lines:
            return None

        return self.store.update_day(date, lines, replace)

    def check(self) -> ty.Iterable[str]:
        yield from self.store.check()

    def migrate(self, new_file: str) -> int:
        cfg = self.cfg.clone()
        cfg[ConfKey.MAIN_LOG_FILE] = new_file
        dst_store = select_store(new_file, cfg)
        dst_store.open()
        res = self.store.migrate(dst_store)
        dst_store.close()
        return res

    def get_projects(
        self,
        ts_begin: datetime.date | None = None,
        ts_end: datetime.date | None = None,
    ) -> ty.Iterable[str]:
        return self.store.get_projects(Context(), ts_begin, ts_end)

    def get_tags(
        self,
        ts_begin: datetime.date | None = None,
        ts_end: datetime.date | None = None,
    ) -> ty.Iterable[str]:
        return self.store.get_tags(Context(), ts_begin, ts_end)


def _prepare_split_parts(inp: str) -> tuple[OptStr, OptStr, str]:
    """
    Split line into:
        - date / datetime / time
        - project
        - rest of message
    """
    # check date
    date: str | None
    date, _, rest = inp.partition(": ")
    if not rest:
        return None, None, inp

    if not parse.is_valid_datetime_or_time(date) and not parse.is_valid_time(
        date
    ):
        date = None
        rest = inp

    # check projct
    if not re.match(r"^(?P<proj>([^W\d_][\w-]*:)+) ", rest):
        return date, None, rest

    project, _, rest = rest.partition(": ")
    return date, project, rest


def prepare_records_from_str(
    inp: str,
    timestamp: datetime.date | datetime.datetime,
    marker: str = "",
) -> ty.Iterable[str]:
    """
    Prepare input string do load.
    1. if timestamp is given - add it to records that not have it
    2. if marker is given - add it after project (if exists) or after date
    """

    is_datetime = isinstance(timestamp, datetime.datetime)
    timestamp_str = timestamp.strftime(
        fmt.TS_FORMAT if is_datetime else fmt.TSD_FORMAT
    )
    date_str = timestamp.strftime(fmt.TSD_FORMAT)
    datetime_str = timestamp.strftime(fmt.TS_FORMAT)

    for line in inp.split("\n"):
        line = line.rstrip()
        # empty lines and lines that start with white character or # are
        # returned as-is; not process it
        if not line or line[0] in " \x09#":
            yield line
            continue

        # replace some common patterns
        if line.startswith("**:"):
            line = datetime_str + line[2:]
        elif line.startswith("*:"):
            line = date_str + line[1:]

        date, project, msg = _prepare_split_parts(line)
        if date:
            if "-" not in date:  # is only time
                date = f"{date_str} {date}"
        else:
            date = timestamp_str

        if marker and not msg.startswith(marker):
            msg = f"{marker} {msg}"

        # remove non-printable characters from msg
        msg = "".join(char for char in msg if char.isprintable())

        yield ": ".join(filter(None, [date, project, msg]))
