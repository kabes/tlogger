# Copyright © 2020 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.
# pylint: disable=protected-access,unspecified-encoding,consider-using-with

"""PlainFileStore tests."""

import os.path
import tempfile
import unittest
from contextlib import suppress

from freezegun import freeze_time

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from . import record, store_file as sf
from .config import Conf, ConfKey
from .model import Context

_T1_CONTENT = """
2019-06-19 15:13: start **
#2019-06-19 15:16: learn: r
2019-06-19 19:38: learn r2
2019-06-19 19:42: start **
2019-06-19 20:04: tlogger: cleanup *
2019-06-19 20:16: learn r2

2019-06-20 13:03: start **
2019-06-20 13:26: tlogger: cleanup
2019-06-20 13:37: tlogger: cleanup
2019-06-20 14:07: start **
"""

_T2_CONTENT = ["2019-06-19 15:13: start **", "2019-06-19 19:38: learn r2"]

_T3_CONTENT = [
    "2019-06-19 15:13: start **",
    "# comments",
    "2019-06-19 19:38: learn r2",
    "",
    "2019-06-19 19:42: start **",
    " ",
]

_T4_CONTENT = """
2019-06-20 13:03: start **
2019-06-20 13:26: tlogger: cleanup
2019-06-20 13:37: tlogger: cleanup
2019-06-20 14:07: start **
2019-06-20: aaa: bbbb
2019-06-20 14:37: tlogger: cleanup
2019-06-20: bbb: aaa

"""

_T5_CONTENT = """
2019-06-19 14:07: start **

2019-06-20: aaa: bbbb
2019-06-20: bbb: aaa

"""


class TestFileOperation(unittest.TestCase):
    def __init__(self, *args, **kwarg) -> None:
        super().__init__(*args, **kwarg)
        self.fpath = None
        self.tmpdir = None

    def setUp(self) -> None:
        self.tmpdir = tempfile.TemporaryDirectory()

    def tearDown(self) -> None:
        self.tmpdir.cleanup()

    def _create_sample(self, content: str = _T1_CONTENT) -> None:
        self.fpath = fpath = os.path.join(self.tmpdir.name, "tlogger.txt")
        with open(fpath, "w", encoding="UTF-8") as fout:
            fout.write(content)

    def test_load_file(self) -> None:
        self._create_sample()
        conf = Conf({ConfKey.MAIN_LOG_FILE: self.fpath})
        pfs = sf.PlainFileStore(conf)
        records = list(pfs.load(Context()))
        self.assertEqual(len(records), 9)

    def test_append(self) -> None:
        self._create_sample()
        conf = Conf({ConfKey.MAIN_LOG_FILE: self.fpath})
        pfs = sf.PlainFileStore(conf)
        records = list(pfs.load(Context()))
        rec = record.from_string(Context(), ["2020-01-01 15:00: test"])
        print(pfs.append(rec))

        pfs = sf.PlainFileStore(conf)
        records = list(pfs.load(Context()))
        self.assertEqual(len(records), 10)
        self.assertEqual(records[-1].human_timestamp(), "2020-01-01 15:00")
        self.assertEqual(records[-1].raw, "2020-01-01 15:00: test")

        rec = record.from_string(Context(), ["2020-01-01 16:00: test2"])
        print(pfs.append(rec))

        pfs = sf.PlainFileStore(conf)
        records = list(pfs.load(Context()))
        self.assertEqual(len(records), 11)
        self.assertEqual(records[-1].human_timestamp(), "2020-01-01 16:00")
        self.assertEqual(records[-1].raw, "2020-01-01 16:00: test2")

        with open(self.fpath) as fin:
            content = fin.readlines()
            self.assertEqual(content[-3].strip(), "")
            self.assertEqual(content[-2].strip(), "2020-01-01 15:00: test")
            self.assertEqual(content[-1].strip(), "2020-01-01 16:00: test2")

    @freeze_time("2019-06-20")
    def test_last_record(self) -> None:
        self._create_sample()
        conf = Conf({ConfKey.MAIN_LOG_FILE: self.fpath})
        pfs = sf.PlainFileStore(conf)
        last = pfs.get_last_record()[0]
        self.assertIsNotNone(last)
        self.assertEqual(last.raw, "2019-06-20 14:07: start **")

        rec = record.from_string(Context(), ["2020-01-01 15:30: test"])
        pfs.append(rec)

        last = pfs.get_last_record()[0]
        self.assertIsNotNone(last)
        self.assertEqual(last.raw, "2020-01-01 15:30: test")

    @freeze_time("2019-06-20")
    def test_last_record2(self) -> None:
        self._create_sample(_T4_CONTENT)
        conf = Conf({ConfKey.MAIN_LOG_FILE: self.fpath})
        pfs = sf.PlainFileStore(conf)
        last, oldest = pfs.get_last_record()
        self.assertIsNotNone(last)
        self.assertIsNotNone(oldest)
        self.assertEqual(last.raw, "2019-06-20: bbb: aaa")
        self.assertEqual(oldest.raw, "2019-06-20 14:37: tlogger: cleanup")

    @freeze_time("2019-06-20")
    def test_last_record3(self) -> None:
        self._create_sample(_T5_CONTENT)
        conf = Conf({ConfKey.MAIN_LOG_FILE: self.fpath})
        pfs = sf.PlainFileStore(conf)
        last, oldest = pfs.get_last_record()
        self.assertIsNotNone(last)
        self.assertIsNotNone(oldest)
        self.assertEqual(last.raw, "2019-06-20: bbb: aaa")
        self.assertEqual(oldest.raw, "2019-06-20: bbb: aaa")

    def test_last_undo(self) -> None:
        self._create_sample()
        conf = Conf({ConfKey.MAIN_LOG_FILE: self.fpath})
        pfs = sf.PlainFileStore(conf)
        pfs.undo()

        with open(self.fpath) as fin:
            content = fin.readlines()
            self.assertEqual(len(content), 11)
            self.assertEqual(
                content[-1].strip(), "2019-06-20 13:37: tlogger: cleanup"
            )

        pfs.undo()
        pfs.undo()
        pfs.undo()
        pfs.undo()

        with open(self.fpath) as fin:
            content = fin.readlines()
            self.assertEqual(len(content), 6)
            self.assertEqual(
                content[-1].strip(), "2019-06-19 20:04: tlogger: cleanup *"
            )
