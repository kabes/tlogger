#! /usr/bin/env python
# vim:fenc=utf-8
#
# Copyright © 2020 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.
"""Parse app data."""

from __future__ import annotations

import datetime
from contextlib import suppress
from functools import lru_cache

from dateutil.relativedelta import relativedelta

# -----------------------------------------------------------
# period conversion
# -----------------------------------------------------------

DateRange = tuple[datetime.date, datetime.date]


def _period_to_date_range_last(drange: str) -> DateRange:
    dtm = today = datetime.date.today()

    match drange:
        case "day":
            dtm -= datetime.timedelta(days=1)
        case "week":
            dtm -= datetime.timedelta(days=7)
        case "month":
            dtm -= relativedelta(months=1)
        case "year":
            dtm -= relativedelta(years=1)
        case _:
            raise ValueError("unknown period")

    return dtm, today


def _period_to_date_range_previous(
    drange: str,
) -> DateRange:
    base_date = datetime.date.today()
    match drange:
        case "day":
            tmax = tmin = base_date - datetime.timedelta(days=1)
        case "week":
            tmax = base_date - datetime.timedelta(days=base_date.weekday() + 1)
            tmin = tmax - datetime.timedelta(days=6)
        case "month":
            tmax = base_date - datetime.timedelta(days=base_date.day)
            tmin = tmax.replace(day=1)
        case "year":
            tmax = base_date.replace(month=1, day=1) - datetime.timedelta(
                days=1
            )
            tmin = tmax.replace(month=1, day=1)
        case _:
            raise ValueError("unknown period")

    return (tmin, tmax)


def _period_to_date_range_this(drange: str) -> DateRange:
    base_date = datetime.date.today()

    match drange:
        case "day":
            dte = dtm = base_date
        case "week":
            dtm = base_date - datetime.timedelta(days=base_date.weekday())
            dte = dtm + datetime.timedelta(days=6)
        case "month":
            dtm = base_date.replace(day=1)
            dte = dtm + relativedelta(months=+1)
            dte -= datetime.timedelta(days=1)
        case "year":
            dtm = base_date.replace(month=1, day=1)
            dte = dtm + relativedelta(years=+1)
            dte -= datetime.timedelta(days=1)
        case _:
            raise ValueError("unknown period")

    return dtm, dte


def _try_parse_date(inp: str, fmt: str) -> datetime.date | None:
    try:
        return datetime.datetime.strptime(inp, fmt).date()
    except ValueError:
        return None


def parse_date_to_range(
    period: str,
) -> tuple[datetime.date, datetime.date] | None:
    """
    Try to guess date range from period as date.
    """

    date: datetime.date | None
    res = None

    with suppress(ValueError):
        offset = int(period)
        if offset < 2000:
            exp_date = (datetime.date.today()) + datetime.timedelta(
                days=offset
            )
            return exp_date, exp_date

    if date := _try_parse_date(period, "%Y-%m-%d"):
        # one day selected
        res = date, date
    elif date := _try_parse_date(period, "%Y-%m"):
        # one month
        dte = date + relativedelta(months=1, days=-1)
        res = date, dte
    elif date := _try_parse_date(period, "%Y"):
        # one month
        dte = date + relativedelta(years=1, days=-1)
        res = date, dte

    return res


def period_to_date_range(period: str) -> DateRange | None:
    """Convert word to time range."""
    if period == "all":
        return None

    if dbegend := parse_date_to_range(period):
        return dbegend

    if period == "today":
        period = "this-day"
    elif period == "yesterday":
        period = "previous-day"

    if not period or "-" not in period:
        raise ValueError(f"unknown period: {period}")

    dselect, _, drange = period.partition("-")

    if dselect == "last":
        return _period_to_date_range_last(drange)

    if dselect == "previous":
        return _period_to_date_range_previous(drange)

    if dselect == "this":
        return _period_to_date_range_this(drange)

    raise ValueError(f"unknown period '{period}'")


def period_to_dates(
    period: str | None,
    begin: datetime.date | None = None,
    end: datetime.date | None = None,
) -> DateRange:
    ts_begin: datetime.date | None = None
    ts_end: datetime.date | None = None
    today = datetime.date.today()

    if period:
        if dates := period_to_date_range(period):
            ts_begin, ts_end = dates

    if begin:
        ts_begin = max(ts_begin or begin, begin)

    if end:
        ts_end = min(ts_end or end, end)

    if not ts_begin and not ts_end:
        return today, today

    ts_begin = ts_begin or min(today, ts_end or today)
    ts_end = ts_end or max(today, ts_begin or today)

    return ts_begin, ts_end


def parse_duration(duration: str | int | None) -> int:
    if not duration:
        raise ValueError("missing duration; expected int, hh:mm or hh:mm:ss")

    if isinstance(duration, int):
        if duration > 0:
            return duration

    else:
        with suppress(ValueError):
            if (val := int(duration)) > 0:
                return val

        with suppress(ValueError):
            tstamp = datetime.datetime.strptime(duration, "%H:%M:%S")
            return tstamp.hour * 60 + tstamp.minute

        with suppress(ValueError):
            tstamp = datetime.datetime.strptime(duration, "%H:%M")
            return tstamp.hour * 60 + tstamp.minute

    raise ValueError("expected number, hh:mm or hh:mm:ss")


_DATE_FORMATS = (
    (False, "%Y-%m-%d"),
    (True, "%Y-%m-%d %H:%M"),
    (False, "%y-%m-%d"),
    (True, "%y-%m-%d %H:%M"),
    (True, "%Y-%m-%d %H:%M:%S"),
    (True, "%y-%m-%d %H:%M:%S"),
)


@lru_cache(1000)
def parse_datetime(inp: str) -> tuple[datetime.datetime, bool]:
    for with_time, date_format in _DATE_FORMATS:
        with suppress(ValueError):
            return datetime.datetime.strptime(inp, date_format), with_time

    raise ValueError("invalid date")


_TIME_FORMATS = ("%H:%M", "%H:%M:%S")


def parse_time(inp: str) -> datetime.time:
    for time_format in _TIME_FORMATS:
        with suppress(ValueError):
            return datetime.datetime.strptime(inp, time_format).time()

    raise ValueError("invalid date")


def parse_date(inp: str) -> datetime.date:
    """
    Convert human formatted date or offset do datetime.date.
    """
    if inp == "today":
        return datetime.date.today()

    if inp == "yesterday":
        return datetime.date.today() - datetime.timedelta(days=1)

    with suppress(ValueError):
        offset = int(inp)
        if offset < 0:
            return datetime.date.today() + datetime.timedelta(days=offset)

    for date_format in ("%Y-%m-%d", "%y-%m-%d"):
        with suppress(ValueError):
            return datetime.datetime.strptime(inp, date_format).date()

    raise ValueError("expected 'yyyy-mm-dd' or 'yy-mm-dd' or today/yesterday")


def parse_ts(inp: str) -> datetime.datetime:
    if not inp:
        raise ValueError("invalid timestamp")

    with suppress(ValueError):
        date, _ = parse_datetime(inp)
        return date

    if inp[0] == "-" and len(inp) > 1:
        with suppress(ValueError):
            dur = parse_duration(inp[1:])
            date = datetime.datetime.now() - datetime.timedelta(minutes=dur)
            return date

    raise ValueError("invalid timestamp")


def is_valid_date(inp: str) -> bool:
    with suppress(ValueError):
        _, with_time = parse_datetime(inp)
        return not with_time

    return False


def is_valid_datetime(inp: str) -> bool:
    with suppress(ValueError):
        _, with_time = parse_datetime(inp)
        return with_time

    return False


def is_valid_datetime_or_time(inp: str) -> bool:
    with suppress(ValueError):
        parse_datetime(inp)
        return True

    return False


def is_valid_time(inp: str) -> bool:
    with suppress(ValueError):
        parse_time(inp)
        return True

    return False
