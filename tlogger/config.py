#! /usr/bin/env python3
# vim:fenc=utf-8
#
# Copyright © 2022 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Application Configuration.
"""
from __future__ import annotations

import configparser
import logging
import os
import typing as ty
from enum import Enum
from pathlib import Path

import click

_LOG = logging.getLogger(__name__)


class PagerMode(Enum):
    AUTO = "auto"
    NEVER = "never"
    ALWAYS = "always"


class ConfKey(Enum):
    MAIN_LOG_FILE = "main.log_file"
    MAIN_TEMPLATE_FILE = "main.template_file"
    MAIN_PAGER = "main.pager"
    FORMAT_DURATION = "format.duration"
    NOTIFY_CMD = "notify.cmd"
    RT_VERBOSE = "_runtime.verbose"


class Conf(dict[ConfKey, ty.Any]):
    def update_not_none(self, other_dict: dict[ConfKey, ty.Any]) -> None:
        for key, value in other_dict.items():
            self.set_not_none(key, value)

    def set_not_none(self, key: ConfKey, value: ty.Any) -> None:
        if value is None:
            return

        if key == ConfKey.MAIN_PAGER:
            self[key] = PagerMode(value)
        else:
            self[key] = value

    def update_path(self, key: ConfKey, fname: str | None) -> None:
        if fname:
            fname = os.path.expanduser(fname)
            self[key] = fname

    def existing_file(self, key: ConfKey) -> str | None:
        if fname := self.get(key):
            if Path(fname).is_file():
                return str(fname)

        return None

    def clone(self) -> Conf:
        return Conf(self.items())


def get_app_file(name: str) -> str:
    return str(Path(click.get_app_dir("tlogger3"), name))


def default_config() -> Conf:
    return Conf(
        {
            ConfKey.MAIN_LOG_FILE: get_app_file("tlogger.txt"),
            ConfKey.MAIN_TEMPLATE_FILE: get_app_file("template.txt"),
            ConfKey.MAIN_PAGER: PagerMode.AUTO,
            ConfKey.FORMAT_DURATION: "human",
            ConfKey.NOTIFY_CMD: "notify-send -u critical "
            "-i notification-message-im "
            "'TLogger: pomodoro time has passed'",
            ConfKey.RT_VERBOSE: False,
        }
    )


def read_config(
    fname: str | None, values: dict[ConfKey, ty.Any] | None = None
) -> Conf:
    fname = fname or get_app_file("config.ini")

    conf = default_config()

    if not Path(fname).is_file():
        _LOG.info("configuration file `%s` not found; using defaults", fname)
        return conf

    _LOG.debug("loading config from `%s`", fname)

    parser = configparser.RawConfigParser()
    parser.read([fname])
    for section in parser.sections():
        for key, value in parser.items(section):
            ckey = ConfKey(f"{section}.{key}")
            if key.endswith("_file"):
                value = os.path.expanduser(value)

            conf.set_not_none(ckey, value)

    if values:
        conf.update_not_none(values)

    return conf


def check_parent_dir(
    fname: str, create: bool = False, must_exists: bool = False
) -> bool:
    dirname = os.path.dirname(fname)
    if not dirname or Path(fname).is_dir():
        return True

    if must_exists and not create:
        raise RuntimeError(f"dir {dirname} not exists")

    if not create:
        return False

    try:
        Path(dirname).mkdir(parents=True, exist_ok=True)
    except OSError as err:
        raise RuntimeError(f"create dir {dirname} error: {err}") from err

    return True
