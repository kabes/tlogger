#! /usr/bin/env python3
# vim:fenc=utf-8
#
# Copyright © 2022 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
ReverseFileReader read file line-by-line backward.
"""
from __future__ import annotations

import os
import types
import typing as ty


class ReverseFileReader:
    """
    Read file line-by-line in reverse order.
    """

    def __init__(self, filename: str):
        """
        Constructor.

        Parameters:
            filename: file name to read
        """
        self.filename = filename
        self._fd: ty.BinaryIO | None = None
        self.block_size = 4096
        self._pos = 0
        self._size = 0
        self._buf = b""
        self._last_pos = 0

    def __enter__(self) -> ReverseFileReader:
        # pylint: disable=consider-using-with
        self._fd = open(self.filename, "rb")
        self._size = self._fd.seek(0, os.SEEK_END)
        self._last_pos = self._size

        # try read file by blocksize, so last block is not full
        first_block = self._size % self.block_size
        if self._fill_buffer(block_size=first_block):
            # skip nl character on end to prevent blank line
            if self._buf[-1] == 10:
                self._buf = self._buf[:-1]

        return self

    def __exit__(
        self,
        exc_type: ty.Type[BaseException] | None,
        exc: BaseException | None,
        traceback: types.TracebackType | None,
    ) -> bool | None:
        if self._fd:
            self._fd.close()

        return exc_type is None

    def __iter__(self) -> ty.Iterator[str | None]:
        return self

    def __next__(self) -> str | None:
        res = self.read()
        if res is None:
            raise StopIteration

        return res

    def _fill_buffer(self, block_size: int = 0) -> int:
        assert self._fd is not None
        block_size = block_size or min(self._last_pos, self.block_size)
        self._last_pos = self._fd.seek(
            self._last_pos - block_size, os.SEEK_SET
        )
        self._buf = self._fd.read(block_size) + self._buf
        return block_size

    def read(self) -> str | None:
        """
        Read and return next line; return None on end of file.
        """
        if not self._fd:
            return None

        if not self._buf and not self._fill_buffer():
            return None

        # load until find nl character
        while b"\n" not in self._buf:
            # but stop if no nl and end of file
            if not self._fill_buffer():
                res = self._buf
                self._buf = b""
                return res.decode()

        # check current buffer
        rnl = self._buf.rfind(b"\n")
        last_line = self._buf[rnl + 1 :] + b"\n"
        self._buf = self._buf[:rnl]
        return last_line.decode("UTF-8")


if __name__ == "__main__":
    with ReverseFileReader("tlogger.txt") as fin:
        # while line := fin.read():
        for line in fin:
            if line:
                print(line.strip())
