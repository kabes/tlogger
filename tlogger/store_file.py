#! /usr/bin/env python3
# vim:fenc=utf-8
#
# Copyright © 2022 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Plain file store
"""
from __future__ import annotations

import datetime
import fcntl
import logging
import os
import re
import shutil
import typing as ty
from contextlib import suppress
from pathlib import Path

from . import filters, formatters as fmt, utils
from .config import Conf, ConfKey
from .errors import LoadError
from .model import Context, InvalidRecord, RecordType
from .record import Record, Records, from_string, load_chunks, parse_tlog_line
from .revfilereader import ReverseFileReader
from .store import AbstractStore

_LOG = logging.getLogger(__name__)


class PlainFileStore(AbstractStore):
    """
    PlainFileStore store records in plain file.

    """

    def __init__(self, cfg: Conf):
        self.fname: str = cfg[ConfKey.MAIN_LOG_FILE]
        self._fd: ty.BinaryIO | None = None

    def open(self) -> None:
        """
        Open store, lock it.
        """
        # pylint: disable=consider-using-with
        lock_file = self.fname + ".lock"
        _LOG.debug("try to create lock `%s`", lock_file)
        self._fd = open(lock_file, "wb")
        if self._fd.writable():
            fcntl.lockf(self._fd, fcntl.LOCK_EX | fcntl.LOCK_NB)

    def close(self) -> None:
        """
        Close store, unlock it.
        """
        if self._fd:
            if self._fd.writable():
                fcntl.lockf(self._fd, fcntl.LOCK_UN)

            self._fd.close()
            Path(self.fname + ".lock").unlink()

    def load(
        self,
        ctx: Context,
        fltr: filters.Filter | None = None,
    ) -> Records:
        """
        Load records from files.

        Args:
            ctx: context
            fltr: optional filters

        Returns:
            Records
        """
        ts_begin = fltr.date_begin if fltr else None
        ts_end = fltr.date_end if fltr else None
        for filename in self._find_files(ts_begin, ts_end):
            _LOG.debug("loading `%s`", filename)
            with open(filename, encoding="UTF-8") as ifile:
                records = from_string(ctx, ifile, filename=filename)

                if fltr:
                    records = fltr.filter_items(records)

                yield from records

    def get_last_record(self) -> tuple[Record | None, Record | None]:
        """
        Get fast last record from current log in the same day.

        Returns:
            (last record in file, oldest record in file)
        """
        fname = self._get_main_file()
        if not fname:
            return None, None

        last_record = None
        oldest_record: Record | None = None
        day_limit = datetime.date.today()
        ctx = Context(validation_enabled=False, fast_load=True)

        _LOG.debug("loading `%s`", fname)
        with ReverseFileReader(fname) as ifile:
            for rec in _parse_lines_headers(ctx, ifile):
                if last_record is None:
                    last_record = rec

                if not oldest_record or rec.end > oldest_record.end:
                    oldest_record = rec

                if rec.day < day_limit or (
                    oldest_record and rec.day < oldest_record.day
                ):
                    break

        return last_record, oldest_record

    def get_last_records_msg(
        self, num: int, *kind: RecordType
    ) -> ty.Iterable[str]:
        """
        Get unique last `num` records message given type.
        Read only current and previous year files (if exists).

        Args:
            num: number of records to load
            kind: list types of records to load

        Returns:
            last messages as str
        """
        files = self._find_files(reverse=True)
        if not files:
            return

        kind = kind or (RecordType.RECORD,)
        messages: list[str] = []
        ctx = Context(validation_enabled=False)
        for fname in files:
            _LOG.debug("loading `%s`", fname)
            with ReverseFileReader(fname) as ifile:
                for rec in _parse_lines_headers(ctx, ifile):
                    if rec.kind not in kind:
                        continue

                    if (rec_msg := rec.full_message) not in messages:
                        messages.append(rec_msg)
                        yield rec_msg
                        if len(messages) == num:
                            return

    def append(self, records: Records) -> tuple[int, int]:
        """
        Appends records into store.

        Args:
            records: records to add

        Returns:
            (number of added records, optional duration from previous record)
            Where number added records exceed 10, add `...` and number of total
            records.
        """

        head, records = utils.peek(records)
        if not head:
            return 0, 0

        first_record = head[0]
        last_record, oldest_record = self.get_last_record()

        last_record_day = last_record.day if last_record else 0
        num_records = 0

        _LOG.debug("opening `%s` for write", self.fname)
        with open(self.fname, "a", encoding="UTF-8") as ofile:
            for rec in records:
                if last_record_day and rec.day != last_record_day:
                    ofile.write("\n")

                last_record_day = rec.day
                ofile.write(rec.raw)
                ofile.write("\n")
                num_records += 1

        if oldest_record:
            duration = (first_record.end - oldest_record.end).total_seconds()
            return num_records, int(duration)

        return num_records, 0

    def list(self) -> ty.Iterable[str]:
        """
        Get content of current file (year).
        """
        _LOG.debug("loading `%s`", self.fname)
        with open(self.fname, encoding="UTF-8") as ofile:
            yield from map(str.strip, ofile.readlines())

    def get_day_records(self, date: datetime.date | None = None) -> Records:
        """
        Get records from current or given day.

        Args:
            data: optional date of records to fetch (default today)
        """
        date = date or datetime.date.today()
        fltr = filters.Filter(date_begin=date, date_end=date)
        return self.load(Context(), fltr=fltr)

    def archive(self) -> ty.Generator[str, None, None]:
        """
        Archive old records.
        """
        fname = self._get_main_file()
        if not fname:
            raise LoadError(f"[W] missing {self.fname} file")

        # make backup
        shutil.copy2(fname, fname + ".bak")

        cur_out_year: int = 0
        cur_out_file: ty.TextIO | None = None
        basename, ext = os.path.splitext(fname)

        _LOG.debug("opening `%s`", fname)
        with open(fname, encoding="UTF-8") as ifile:
            ctx = Context()
            for rec in from_string(ctx, ifile, filename=fname, preserve=True):
                if rec.kind == RecordType.OTHER:
                    if cur_out_file:
                        cur_out_file.write(rec.raw)
                        cur_out_file.write("\n")

                    continue

                year = rec.end.year
                if year == cur_out_year:
                    assert cur_out_file
                    cur_out_file.write(rec.raw)
                    cur_out_file.write("\n")
                    continue

                if cur_out_file:
                    cur_out_file.close()

                yfname = f"{basename}_{year}{ext}"
                yield f"[D] writing year {year} info {yfname}"
                # pylint: disable=consider-using-with

                _LOG.debug("opening `%s` for write", yfname)
                cur_out_file = open(yfname, "w+t", encoding="UTF-8")
                cur_out_year = year
                cur_out_file.write(rec.raw)
                cur_out_file.write("\n")

        if cur_out_file:
            cur_out_file.close()

        cur_year_fname = f"{basename}_{year}{ext}"
        if Path(cur_year_fname).is_file():
            shutil.move(cur_year_fname, fname)

        yield "Done"

    def undo(self) -> str:
        """
        Remove last record from file.

        Return:
            removed record.
        """

        fname = self._get_main_file()
        if not fname:
            return ""

        prev: list[str] = []
        with open(f"{fname}.new", "w", encoding="UTF-8") as ofile, open(
            fname, encoding="UTF-8"
        ) as ifile:
            for _, _, chunk in load_chunks(ifile, preserve=True):
                for line in prev:
                    ofile.write(line)
                    ofile.write("\n")

                prev = chunk

        os.rename(fname, f"{fname}.bak")
        os.rename(f"{fname}.new", fname)

        return "\n".join(prev)

    def update_day(
        self, date: datetime.date, content: ty.Iterable[str], replace: bool
    ) -> None:
        """
        Replace entries from given day by given.
        """
        records = "\n".join(
            rec.raw for rec in from_string(Context(), content, sort=True)
        )
        if not records:
            return

        records += "\n"

        fname = self._get_main_file()
        if not fname:
            Path(self.fname).write_text(records, encoding="UTF-8")
            return

        date_str = date.strftime(fmt.TSD_FORMAT)
        inserted = False  # records was save
        prev_has_blank = True  # previous chunks ends with new line

        with open(f"{fname}.new", "w", encoding="UTF-8") as ofile, open(
            fname, encoding="UTF-8"
        ) as ifile:
            for _, _, chunk in load_chunks(ifile, preserve=True):
                header = chunk[0]
                if header.startswith(date_str) and replace:
                    continue

                if not inserted and header > date_str:  # hack
                    ofile.write(records)
                    inserted = True
                    ofile.write("\n")

                for line in chunk:
                    ofile.write(line)
                    ofile.write("\n")

                prev_has_blank = chunk[-1] == ""

            if not inserted:
                if not prev_has_blank:
                    ofile.write("\n")
                ofile.write(records)

        os.rename(fname, f"{fname}.bak")
        os.rename(f"{fname}.new", fname)

    def check(self) -> ty.Iterable[str]:
        """
        Check storage.
        """
        filename = self._get_main_file()
        if not filename:
            return

        _LOG.debug("opening `%s`", filename)
        with open(filename, encoding="UTF-8") as ifile:
            for fname, _, chunk in load_chunks(ifile, filename=filename):
                try:
                    parse_tlog_line(Context(), chunk[0], chunk[1:])
                except InvalidRecord as err:
                    yield f"E:{fname}:{fname}:{err}"

    def _get_main_file(self) -> str | None:
        if os.path.isfile(self.fname):
            return self.fname

        return None

    def _find_files(
        self,
        ts_begin: datetime.date | None = None,
        ts_end: datetime.date | None = None,
        reverse: bool = False,
    ) -> ty.Iterable[str]:
        year_files: ty.Iterable[tuple[int, str]] = sorted(
            get_all_files(self.fname)
        )
        _LOG.debug("found files: %s", year_files)

        if ts_begin:
            begin_year = ts_begin.year
            year_files = filter(lambda yfp: yfp[0] >= begin_year, year_files)

        if ts_end:
            end_year = ts_end.year
            year_files = filter(lambda yfp: yfp[0] <= end_year, year_files)

        file_names: list[str] = [name for _, name in year_files]

        if reverse:
            file_names.reverse()

        _LOG.debug("accepted files: %s", file_names)
        return file_names


def get_all_files(base_fname: str) -> ty.Iterable[tuple[int, str]]:
    bdir = os.path.dirname(base_fname) or "."
    bname = os.path.basename(base_fname)
    fname, fext = os.path.splitext(bname)

    regex = re.compile("^" + fname + r"(_(\d{4}))?" + fext + "$")

    for name in os.listdir(bdir):
        if rmt := regex.match(name):
            full_path = Path(bdir, name)
            if full_path.is_file():
                yield int(rmt.group(2) or datetime.date.today().year), str(
                    full_path
                )


def _parse_lines_headers(
    ctx: Context, lines: ty.Iterable[str | None]
) -> Records:
    """
    Parse lines into records, skipping not-header and invalid records.
    """
    for line in lines:
        if not line:
            continue

        line = line.strip()
        if not line or line[0] in (" ", "\x09", "#"):
            continue

        with suppress(InvalidRecord):
            yield parse_tlog_line(ctx, line)
