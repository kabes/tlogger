# Copyright © 2020 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.
# pylint: disable=protected-access,unspecified-encoding,consider-using-with

"""PlainFileStore tests."""

import unittest
from contextlib import suppress

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from freezegun import freeze_time

from . import record, store_sqlite as ss
from .config import ConfKey
from .model import Context


class TestFilters(unittest.TestCase):
    def test_parse_args(self):
        sql, args = ss._parse_args("col", [])
        self.assertEqual((sql, args), ("", []))

        sql, args = ss._parse_args("col", ["test"])
        self.assertEqual(sql, "(col in (?))")
        self.assertEqual(args, ["test"])

        sql, args = ss._parse_args("col", ["test", "test2"])
        self.assertEqual(sql, "(col in (?,?))")
        self.assertEqual(args, ["test", "test2"])

        sql, args = ss._parse_args("col", ["test*"])
        self.assertEqual(sql, "(col like ?)")
        self.assertEqual(args, ["test%"])

        sql, args = ss._parse_args("col", ["test*", "test2*"])
        self.assertEqual(sql, "(col like ? or col like ?)")
        self.assertEqual(args, ["test%", "test2%"])

        sql, args = ss._parse_args(
            "col", ["test*", "test2*", "test3", "test4"]
        )
        self.assertEqual(sql, "(col in (?,?) or col like ? or col like ?)")
        self.assertEqual(args, ["test3", "test4", "test%", "test2%"])


_T1_CONTENT = """
2019-06-19 15:13: start **
#2019-06-19 15:16: learn: r
2019-06-19 19:38: learn r2
2019-06-19 19:42: start **
2019-06-19 20:04: tlogger: cleanup *
2019-06-19 20:16: learn r2

2019-06-20 13:03: start **
2019-06-20 13:26: tlogger: cleanup
2019-06-20 13:37: tlogger: cleanup
2019-06-20 14:07: start **
"""

_T2_CONTENT = ["2019-06-19 15:13: start **", "2019-06-19 19:38: learn r2"]

_T3_CONTENT = [
    "2019-06-19 15:13: start **",
    "# comments",
    "2019-06-19 19:38: learn r2",
    "",
    "2019-06-19 19:42: start **",
    " ",
]

_T4_CONTENT = """
2019-06-20 13:03: start **
2019-06-20 13:26: tlogger: cleanup
2019-06-20 13:37: tlogger: cleanup
2019-06-20 14:07: start **
2019-06-20: aaa: bbbb
2019-06-20 14:37: tlogger: cleanup
2019-06-20: bbb: aaa
"""

_T5_CONTENT = """
2019-06-19 14:07: start **
2019-06-20: aaa: bbbb
2019-06-20: bbb: aaa
"""


class TestSqliteStore(unittest.TestCase):
    def setUp(self):
        self.store = ss.SqliteStore({ConfKey.MAIN_LOG_FILE: ":memory:"})
        self.store.open()

    def tearDown(self):
        self.store.close()

    def _create_sample(self, content: str = _T1_CONTENT) -> None:
        ctx = Context()
        for line in content.splitlines():
            if not line:
                continue

            rec = record.from_string(ctx, [line])
            print(self.store.append(rec))

    def test_append(self):
        self._create_sample()

        rec = record.from_string(Context(), ["2020-01-01 15:00: test"])
        print(self.store.append(rec))

        records = list(self.store.load(Context()))
        self.assertEqual(len(records), 10)
        self.assertEqual(records[-1].human_timestamp(), "2020-01-01 15:00")
        self.assertEqual(records[-1].raw, "2020-01-01 15:00: test")

        rec = record.from_string(Context(), ["2020-01-01 16:00: test2"])
        print(self.store.append(rec))

        records = list(self.store.load(Context()))
        self.assertEqual(len(records), 11)
        self.assertEqual(records[-1].human_timestamp(), "2020-01-01 16:00")
        self.assertEqual(records[-1].raw, "2020-01-01 16:00: test2")

        content = list(self.store.list_all())
        print(content)
        self.assertEqual(content[-3].strip(), "2019-06-20 14:07: start **")
        self.assertEqual(content[-2].strip(), "2020-01-01 15:00: test")
        self.assertEqual(content[-1].strip(), "2020-01-01 16:00: test2")

    @freeze_time("2019-06-20")
    def test_last_record(self) -> None:
        self._create_sample()
        last = self.store.get_last_record()[0]
        self.assertIsNotNone(last)
        self.assertEqual(last.raw, "2019-06-20 14:07: start **")

        rec = record.from_string(Context(), ["2020-01-01 15:30: test"])
        self.store.append(rec)

        last = self.store.get_last_record()[0]
        print(last)
        self.assertIsNotNone(last)
        self.assertEqual(last.raw, "2020-01-01 15:30: test")

    @freeze_time("2019-06-20")
    def test_last_record2(self) -> None:
        self._create_sample(_T4_CONTENT)
        last, oldest = self.store.get_last_record()
        self.assertIsNotNone(last)
        self.assertIsNotNone(oldest)
        self.assertEqual(last.raw, "2019-06-20: bbb: aaa")
        self.assertEqual(oldest.raw, "2019-06-20 14:37: tlogger: cleanup")

    @freeze_time("2019-06-20")
    def test_last_record3(self) -> None:
        self._create_sample(_T5_CONTENT)
        print(list(self.store.list_all()))
        last, oldest = self.store.get_last_record()
        self.assertIsNotNone(last)
        self.assertIsNotNone(oldest)
        self.assertEqual(last.raw, "2019-06-20: bbb: aaa")
        self.assertEqual(oldest.raw, "2019-06-20: bbb: aaa")

    def test_last_undo(self) -> None:
        self._create_sample()
        self.store.undo()

        content = list(self.store.list_all())
        self.assertEqual(len(content), 8)
        self.assertEqual(
            content[-1].strip(), "2019-06-20 13:37: tlogger: cleanup"
        )

        self.store.undo()
        self.store.undo()
        self.store.undo()
        self.store.undo()

        content = list(self.store.list_all())
        self.assertEqual(len(content), 4)
        self.assertEqual(
            content[-1].strip(), "2019-06-19 20:04: tlogger: cleanup *"
        )
