#! /usr/bin/env python
# vim:fenc=utf-8
#
# Copyright © 2020 Karol Będkowski <Karol Będkowski@kntbk>
#
# Distributed under terms of the GPLv3 license.
# pylint: disable=protected-access

"""Filters tests."""

import datetime
import unittest
from contextlib import suppress

from freezegun import freeze_time

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)


from . import filters, record
from .model import Context

_T2_CONTENT = """
2019-06-19 15:13: start **
#2019-06-19 15:16: learn: r @cont2 +tag1  # !!!!!!!!
2019-06-19 19:38: learn: r2 @cont1  @cont3
2019-06-19 19:42: start **
2019-06-19 20:04: tlogger: cleanup * @cont3
2019-06-19 20:16: learn: r2

2019-06-20 13:03: start **
2019-06-20 13:26: tlogger: cleanup +tag2
2019-06-20 13:37: tlogger: cleanup +tag1 +tag2
2019-06-20 14:07: start **
"""

_T3_CONTENT = """
2019-06-19 15:13: start **
#2019-06-19 15:16: learn: r @cont2 +tag1  # !!!!!!!!
2019-06-19 19:38: learn: r2 @cont1  @cont3
2019-06-19 19:42: start **
2019-06-19 20:04: tlogger: cleanup * @cont3
2019-06-19 20:16: learn: r2

2019-06-20 13:03: start **
2019-06-20 13:26: tlogger: cleanup +tag2
2019-06-20 13:37: tlogger: cleanup +tag1 +tag2
2019-06-20 14:07: start **
2019-06-20: # note 1
2019-06-20 18:23: # note 2
2019-06-20: cnt: +1

2019-06-21 13:26: tlogger: clean21 +tag2
2019-06-21 13:37: tlogger: clean21 +tag1 +tag2
2019-06-21 14:07: start **
2019-06-21: note 3
2019-06-21 18:02: cnt: +2 cntr with note
"""


def _create_records(content: str = _T2_CONTENT) -> list[record.Record]:
    return list(record.from_string(Context(), content.split("\n")))


class TestFilters(unittest.TestCase):
    def test_filter_by_words(self) -> None:
        records = _create_records()
        self.assertEqual(
            list(filters.filter_items_by_words(records, None)), records
        )
        self.assertEqual(
            list(filters.filter_items_by_words(records, [])), records
        )

        res = list(filters.filter_items_by_words(records, ["cleanup"]))
        self.assertEqual(len(res), 3)
        self.assertEqual(
            res[0].raw, "2019-06-19 20:04: tlogger: cleanup * @cont3"
        )
        self.assertEqual(
            res[1].raw, "2019-06-20 13:26: tlogger: cleanup +tag2"
        )
        self.assertEqual(
            res[2].raw, "2019-06-20 13:37: tlogger: cleanup +tag1 +tag2"
        )

        res = list(filters.filter_items_by_words(records, ["cleanup", "tag1"]))
        self.assertEqual(len(res), 1)
        self.assertEqual(
            res[0].raw, "2019-06-20 13:37: tlogger: cleanup +tag1 +tag2"
        )

    def test_filter_by_words_neg(self) -> None:
        records = _create_records()
        self.assertEqual(len(records), 9)

        res = list(filters.filter_items_by_words(records, ["not:cleanup"]))
        self.assertEqual(len(res), 6)

        res = list(
            filters.filter_items_by_words(records, ["not:cleanup", "cont3"])
        )
        self.assertEqual(len(res), 1)

    def test_filter_by_project(self) -> None:
        records = _create_records()
        self.assertEqual(
            list(filters.filter_items_by_project(records, None)), records
        )
        self.assertEqual(
            list(filters.filter_items_by_project(records, [])), records
        )

        res = list(filters.filter_items_by_project(records, ["tlogger"]))
        self.assertEqual(len(res), 3)
        self.assertEqual(
            res[0].raw, "2019-06-19 20:04: tlogger: cleanup * @cont3"
        )
        self.assertEqual(
            res[1].raw, "2019-06-20 13:26: tlogger: cleanup +tag2"
        )
        self.assertEqual(
            res[2].raw, "2019-06-20 13:37: tlogger: cleanup +tag1 +tag2"
        )

        res = list(filters.filter_items_by_words(records, ["learn"]))
        self.assertEqual(len(res), 2)
        self.assertEqual(
            res[0].raw, "2019-06-19 19:38: learn: r2 @cont1  @cont3"
        )
        self.assertEqual(res[1].raw, "2019-06-19 20:16: learn: r2")

    def test_filter_by_project_neg(self) -> None:
        records = _create_records()
        res = list(filters.filter_items_by_project(records, ["not:tlogger"]))
        self.assertEqual(len(res), 6)

    def test_filter_by_project_wc(self) -> None:
        records = _create_records()
        res = list(filters.filter_items_by_project(records, ["tlog*"]))
        self.assertEqual(len(res), 3)

    def test_filter_by_tags(self) -> None:
        records = _create_records()
        self.assertEqual(
            list(filters.filter_items_by_tags(records, None)), records
        )
        self.assertEqual(
            list(filters.filter_items_by_tags(records, [])), records
        )
        self.assertEqual(
            list(filters.filter_items_by_tags(records, ["  "])), records
        )

        self.assertEqual(
            list(filters.filter_items_by_tags(records, ["unkn"])), []
        )

        res = list(filters.filter_items_by_tags(records, ["tag1"]))
        self.assertEqual(len(res), 1)
        self.assertEqual(
            res[0].raw, "2019-06-20 13:37: tlogger: cleanup +tag1 +tag2"
        )

        res = list(filters.filter_items_by_tags(records, ["tag2"]))
        self.assertEqual(len(res), 2)
        self.assertEqual(
            res[0].raw, "2019-06-20 13:26: tlogger: cleanup +tag2"
        )
        self.assertEqual(
            res[1].raw, "2019-06-20 13:37: tlogger: cleanup +tag1 +tag2"
        )

        res = list(filters.filter_items_by_tags(records, ["tag2", "tag1"]))
        self.assertEqual(len(res), 1)
        self.assertEqual(
            res[0].raw, "2019-06-20 13:37: tlogger: cleanup +tag1 +tag2"
        )

        res = list(filters.filter_items_by_tags(records, ["+tag1"]))
        self.assertEqual(len(res), 1)
        self.assertEqual(
            res[0].raw, "2019-06-20 13:37: tlogger: cleanup +tag1 +tag2"
        )

        res = list(filters.filter_items_by_tags(records, ["+tag2"]))
        self.assertEqual(len(res), 2)
        self.assertEqual(
            res[0].raw, "2019-06-20 13:26: tlogger: cleanup +tag2"
        )
        self.assertEqual(
            res[1].raw, "2019-06-20 13:37: tlogger: cleanup +tag1 +tag2"
        )

        res = list(filters.filter_items_by_tags(records, ["+tag2", "tag1"]))
        self.assertEqual(len(res), 1)
        self.assertEqual(
            res[0].raw, "2019-06-20 13:37: tlogger: cleanup +tag1 +tag2"
        )

    def test_filter_by_tags_neg(self) -> None:
        records = _create_records()

        res = list(filters.filter_items_by_tags(records, ["not:+tag2"]))
        self.assertEqual(len(res), 7)

        res = list(
            filters.filter_items_by_tags(records, ["+tag2", "not:+tag1"])
        )
        self.assertEqual(len(res), 1)

    def test_filter_by_period(self) -> None:
        records = _create_records()
        self.assertEqual(
            list(filters.filter_items_by_period(records, None)), records
        )

        with self.assertRaises(ValueError):
            filters.filter_items_by_period(records, "unkn")

        with freeze_time("2019-06-20"):
            res = list(filters.filter_items_by_period(records, "yesterday"))

        self.assertEqual(len(res), 5)
        self.assertEqual(res[0].raw, "2019-06-19 15:13: start **")
        self.assertEqual(
            res[1].raw, "2019-06-19 19:38: learn: r2 @cont1  @cont3"
        )
        self.assertEqual(res[2].raw, "2019-06-19 19:42: start **")
        self.assertEqual(
            res[3].raw, "2019-06-19 20:04: tlogger: cleanup * @cont3"
        )
        self.assertEqual(res[4].raw, "2019-06-19 20:16: learn: r2")

        with freeze_time("2019-06-22"):
            res = list(filters.filter_items_by_period(records, "yesterday"))
        self.assertEqual(len(res), 0)

    def test_filter_by_dates(self) -> None:
        self.assertEqual(list(filters.filter_items_by_dates([], None)), [])
        records = _create_records()
        self.assertEqual(
            list(filters.filter_items_by_dates(records, None)), records
        )
        self.assertEqual(
            list(filters.filter_items_by_dates(records, None, None)), records
        )
        self.assertEqual(
            list(filters.filter_items_by_dates(records, until=None)), records
        )

        res = list(
            filters.filter_items_by_dates(
                records, datetime.date(2019, 6, 19), datetime.date(2019, 6, 19)
            )
        )
        self.assertEqual(len(res), 5)
        self.assertEqual(res[0].raw, "2019-06-19 15:13: start **")
        self.assertEqual(
            res[1].raw, "2019-06-19 19:38: learn: r2 @cont1  @cont3"
        )
        self.assertEqual(res[2].raw, "2019-06-19 19:42: start **")
        self.assertEqual(
            res[3].raw, "2019-06-19 20:04: tlogger: cleanup * @cont3"
        )
        self.assertEqual(res[4].raw, "2019-06-19 20:16: learn: r2")

        res = list(
            filters.filter_items_by_dates(
                records, datetime.date(2018, 6, 19), datetime.date(2019, 6, 19)
            )
        )
        self.assertEqual(len(res), 5)

        res = list(
            filters.filter_items_by_dates(
                records, datetime.date(2019, 6, 19), datetime.date(2019, 6, 20)
            )
        )
        self.assertEqual(len(res), 9)

        res = list(
            filters.filter_items_by_dates(
                records, datetime.date(2019, 6, 20), datetime.date(2020, 6, 21)
            )
        )
        self.assertEqual(len(res), 4)

    def test_filter_by_types(self) -> None:
        self.assertEqual(list(filters.filter_items_by_type([], None)), [])

        records = _create_records(_T3_CONTENT)
        self.assertEqual(
            list(filters.filter_items_by_type(records, None)), records
        )
        self.assertEqual(
            list(filters.filter_items_by_type(records, ["all"])), records
        )

        self.assertEqual(
            len(list(filters.filter_items_by_type(records, ["counter"]))), 2
        )

        self.assertEqual(
            len(list(filters.filter_items_by_type(records, ["note"]))), 4
        )

        self.assertEqual(
            len(list(filters.filter_items_by_type(records, ["record"]))), 7
        )
        self.assertEqual(
            len(list(filters.filter_items_by_type(records, ["marker"]))), 5
        )

        self.assertEqual(
            len(
                list(
                    filters.filter_items_by_type(records, ["marker", "record"])
                )
            ),
            12,
        )

        self.assertEqual(
            len(
                list(
                    filters.filter_items_by_type(records, ["note", "counter"])
                )
            ),
            5,
        )


class TestFilter(unittest.TestCase):
    def test_filte_date_ranges1(self) -> None:
        fltr = filters.Filter(
            date_begin=datetime.date(2023, 3, 5),
            date_end=datetime.date(2023, 5, 20),
        )
        min_d, max_d = fltr.real_date_ranges()
        self.assertEqual(min_d, datetime.date(2023, 3, 5))
        self.assertEqual(max_d, datetime.date(2023, 5, 20))

    @freeze_time("2023-04-05")
    def test_filte_date_ranges2(self) -> None:
        fltr = filters.Filter(
            date_begin=datetime.date(2023, 3, 5),
            date_end=datetime.date(2023, 5, 20),
            period="previous-month",
        )
        min_d, max_d = fltr.real_date_ranges()
        self.assertEqual(min_d, datetime.date(2023, 3, 5))
        self.assertEqual(max_d, datetime.date(2023, 3, 31))

    @freeze_time("2023-04-05")
    def test_filte_date_ranges3(self) -> None:
        fltr = filters.Filter(
            period="previous-month",
        )
        min_d, max_d = fltr.real_date_ranges()
        self.assertEqual(min_d, datetime.date(2023, 3, 1))
        self.assertEqual(max_d, datetime.date(2023, 3, 31))
