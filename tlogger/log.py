#! /usr/bin/env python3
# vim:fenc=utf-8
#
# Copyright © 2022 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Logging setup.
"""
import logging
import sys


class ColorFormatter(logging.Formatter):
    """Formatter for logs that color messages according to level."""

    FORMAT_MAP = {
        levelno: f"\033[1;{color}m{level:<8}\033[0m"
        for levelno, level, color in (
            (logging.DEBUG, "DEBUG", 34),
            (logging.INFO, "INFO", 37),
            (logging.WARNING, "WARNING", 33),
            (logging.ERROR, "ERROR", 31),
            (logging.CRITICAL, "CRITICAL", 31),
        )
    }

    def format(self, record: logging.LogRecord) -> str:
        record.levelname = self.FORMAT_MAP.get(
            record.levelno, record.levelname
        )
        return logging.Formatter.format(self, record)


def setup_logging(level: int) -> None:
    log = logging.getLogger(None)
    if level >= 2:
        # debug
        log.setLevel(logging.DEBUG)
    elif level == 1:
        # verbose
        log.setLevel(logging.INFO)
    else:
        log.setLevel(logging.WARN)

    console = logging.StreamHandler()
    fmtr = logging.Formatter
    if sys.platform != "win32":
        fmtr = ColorFormatter

    if level == 2:
        msg_format = (
            "%(levelname)-8s %(name)s [%(filename)s:%(lineno)d] %(message)s"
        )
    else:
        msg_format = "%(message)s"

    console.setFormatter(fmtr(msg_format))
    log.addHandler(console)

    logging.getLogger("main").info("ok")
