#! /usr/bin/env python3
# vim:fenc=utf-8
#
# Copyright © 2022 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Common utility functions
"""

import itertools
import typing as ty

_T = ty.TypeVar("_T")


def peek(
    iterable: ty.Iterable[_T], num: int = 1
) -> tuple[list[_T], ty.Iterator[_T]]:
    """
    Get list of first `num` items from iterable. Return also origin iterable.
    """
    iterable = iter(iterable)
    head = list(itertools.islice(iterable, num))
    return head, itertools.chain(head, iterable)


def call_every_n(
    iterable: ty.Iterable[_T],
    num: int,
    callback: ty.Callable[[int], None],
) -> ty.Iterator[_T]:
    """Iterate over `iterable`, execute `callback` every `num` items; return
    item from iterable.

    callback get total number processed so far elements as argument.
    """
    counter = itertools.count()
    selector = itertools.cycle(range(num))
    for item, total, cntr in zip(iterable, counter, selector):
        if not cntr:
            callback(total)

        yield item
