#! /usr/bin/env python
# vim:fenc=utf-8
#
# Copyright © 2020 Karol Będkowski <Karol Będkowski@kntbk>
#
# Distributed under terms of the GPLv3 license.

"""Common filters."""
from __future__ import annotations

import datetime
import itertools
import typing as ty
from dataclasses import dataclass

from . import parse
from .model import RecordType
from .record import Record, Records

TyOptionalArg = ty.Optional[ty.Iterable[str]]


def split_pos_neg(
    words: ty.Iterable[str],
) -> tuple[set[str], set[str]]:
    words = filter(None, (word.strip() for word in words))
    words1, words2 = itertools.tee(words)
    pos_words = {
        word.strip() for word in words1 if word and not word.startswith("not:")
    }
    neg_words = {
        word[4:].strip()
        for word in words2
        if word.startswith("not:") and len(word) > 4
    }
    return pos_words, neg_words


def filter_items_by_words(
    items: ty.Iterable[Record], words: TyOptionalArg
) -> ty.Iterable[Record]:
    if not words:
        return items

    # unique
    pos_words, neg_words = split_pos_neg(words)

    if pos_words:
        items = (
            item
            for item in items
            if all(word in item.raw for word in pos_words)
        )

    if neg_words:
        items = (
            item
            for item in items
            if not any(word in item.raw for word in neg_words)
        )

    return items


def _project_matcher(project: str) -> ty.Callable[[Record], bool]:
    if project.endswith("*"):
        project = project.rstrip("*")
        return lambda rec: bool(rec.project) and rec.project.startswith(
            project
        )

    project = project.rstrip(":")
    return lambda rec: rec.project == project


def filter_items_by_project(
    items: ty.Iterable[Record], projects: TyOptionalArg
) -> ty.Iterable[Record]:
    if not projects:
        return items

    # unique
    pos_proj, neg_proj = split_pos_neg(projects)
    pos_matchers = [_project_matcher(proj) for proj in pos_proj]
    neg_matchers = [_project_matcher(proj) for proj in neg_proj]

    if pos_proj:
        items = (rec for rec in items if any(mat(rec) for mat in pos_matchers))

    if neg_proj:
        items = (
            rec for rec in items if not any(mat(rec) for mat in neg_matchers)
        )

    return items


def filter_items_by_tags(
    items: ty.Iterable[Record], tags: TyOptionalArg
) -> ty.Iterable[Record]:
    """Filter items by tags.

    All returned items must have all given tags.
    If tags not starts with '+' - it is added.
    """
    if not tags or not items:
        return items

    pos_tags, neg_tags = split_pos_neg(tags)

    if pos_tags:
        adj_pos_tags = tuple(
            tag if tag[0] == "+" else "+" + tag
            for tag in pos_tags
            if tag != "+"
        )
        items = (
            item
            for item in items
            if item.tags and all(t in item.tags for t in adj_pos_tags)
        )

    if neg_tags:
        adj_neg_tags = tuple(
            tag if tag[0] == "+" else "+" + tag
            for tag in neg_tags
            if tag != "+"
        )
        items = (
            item
            for item in items
            if not item.tags or all(t not in item.tags for t in adj_neg_tags)
        )

    return items


def filter_items_by_period(
    items: ty.Iterable[Record],
    period: str | None,
) -> ty.Iterable[Record]:
    if not period:
        return items

    if dminmax := parse.period_to_date_range(period):
        dmin, dmax = dminmax
        if dmin:
            dmin_dt = datetime.datetime.combine(
                dmin, datetime.datetime.min.time()
            )
            items = filter(lambda x: x.end >= dmin_dt, items)

        if dmax:
            dmax_dt = datetime.datetime.combine(
                dmax, datetime.datetime.max.time()
            )
            items = filter(lambda x: x.end < dmax_dt, items)

    return items


def filter_items_by_dates(
    items: ty.Iterable[Record],
    since: datetime.date | None = None,
    until: datetime.date | None = None,
) -> ty.Iterable[Record]:
    if not items:
        return items

    if since:
        since_dt = datetime.datetime.combine(
            since, datetime.datetime.min.time()
        )
        items = filter(lambda x: x.end >= since_dt, items)

    if until:
        until_dt = datetime.datetime.combine(
            until, datetime.datetime.max.time()
        )
        items = filter(lambda x: x.end <= until_dt, items)

    return items


def filter_items_by_type(
    items: ty.Iterable[Record], types: list[str] | None
) -> ty.Iterable[Record]:
    if not items or not types or "all" in types:
        return items

    rtypes: list[RecordType] = [RecordType(typ) for typ in types]

    def _filter(rec: Record) -> bool:
        return any(rec.is_type(rtype) for rtype in rtypes)

    return filter(_filter, items)


@dataclass(slots=True)
class Filter:
    date_begin: datetime.date | None = None
    date_end: datetime.date | None = None
    types: list[str] | None = None
    period: str | None = None
    words: TyOptionalArg = None
    projects: TyOptionalArg = None
    tags: TyOptionalArg = None

    def filter_items(self, records: Records) -> Records:
        records = filter_items_by_dates(
            records, self.date_begin, self.date_end
        )
        records = filter_items_by_type(records, self.types)
        records = filter_items_by_period(records, self.period)
        records = filter_items_by_words(records, self.words)
        records = filter_items_by_project(records, self.projects)
        records = filter_items_by_tags(records, self.tags)
        return records

    def is_match(self, record: Record) -> bool:
        records = self.filter_items([record])
        return bool(list(records))

    def real_date_ranges(
        self,
    ) -> tuple[datetime.date | None, datetime.date | None]:
        """Find period that cover min_date - max_date and given period."""
        min_date = self.date_begin
        max_date = self.date_end

        if not self.period:
            return min_date, max_date

        if dminmax := parse.period_to_date_range(self.period):
            dmin, dmax = dminmax
            if dmin and (not min_date or dmin > min_date):
                min_date = dmin

            if dmax and (not max_date or dmax < max_date):
                max_date = dmax

        return min_date, max_date
