# Copyright © 2023 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Report tests
"""
import datetime
import unittest
from contextlib import suppress

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from . import record, reports
from .model import Context


class TestGenerateReportSplit(unittest.TestCase):
    def test0(self) -> None:
        items = [
            [["123", "12"], "2", "3"],
            [["123", "23"], "2 2"],
            [["120", "33"], "4", "5", "1234"],
            [["a"], "777"],
        ]
        cols = ["a", "b", "c"]

        def fmtr(new_items, cols, align, opts):
            return "\n".join(map(lambda x: ";".join(x), new_items))

        res = reports._generate_report_split(items, cols, fmtr, None, None)
        ic(res)
        rows = res.split("\n")
        self.assertEqual(rows[0], "123;12;2;3")
        self.assertEqual(rows[1], "123;23;2 2")
        self.assertEqual(rows[2], "120;33;4;5;1234")
        self.assertEqual(rows[3], "a;;777")


_T1_CONTENT = """
2019-06-19 15:13: start **
#2019-06-19 15:16: learn: r
2019-06-19 19:38: learn r2
2019-06-19 19:42: start **
2019-06-19 20:04: tlogger: cleanup *
2019-06-19 20:16: learn r2
2019-06-19 20:37: project1: test

2019-06-20 13:03: start **
2019-06-20 13:26: tlogger: cleanup
2019-06-20 13:37: tlogger: cleanup
2019-06-20 14:07: start **
2019-06-20 14:37: project2: test

2019-06-21 12:00: start **
2019-06-21 13:00: project2: test2

2019-07-10 12:00: start **
2019-07-10 12:20: tlogger: cleanup
2019-07-10 13:00: project2: test2

2020-05-11 10:00: start **
2020-05-11 11:20: tlogger: cleanup
2020-05-11 12:00: start **
2020-05-11 13:00: project2: test2
2020-05-11 13:30: project1: test
"""


class TestGroupItems(unittest.TestCase):
    def test_year(self) -> None:
        records = list(record.from_string(Context(), _T1_CONTENT.split("\n")))
        res = list(reports.group_items(records, ["year"], 0))
        ic(res)
        self.assertEqual(len(res), 3)

        label, group = res[0]
        self.assertEqual(label, ("2019",))
        self.assertEqual(group.min_ts, datetime.datetime(2019, 6, 19, 15, 13))
        self.assertEqual(group.max_ts, datetime.datetime(2019, 7, 10, 13, 0))
        self.assertEqual(group.group_days, 365)
        self.assertEqual(group.count, 16)
        self.assertEqual(group.duration, 513)

        label, group = res[1]
        self.assertEqual(label, ("2020",))
        self.assertEqual(group.count, 5)
        self.assertEqual(group.duration, 173)
        self.assertEqual(group.min_ts, datetime.datetime(2020, 5, 11, 10, 0))
        self.assertEqual(group.max_ts, datetime.datetime(2020, 5, 11, 13, 30))
        self.assertEqual(group.group_days, 366)

        label, group = res[2]
        self.assertEqual(label, ("<total>",))
        self.assertEqual(group.count, 21)
        self.assertEqual(group.group_days, 0)
        self.assertEqual(group.duration, 686)

    def test_project(self) -> None:
        records = list(record.from_string(Context(), _T1_CONTENT.split("\n")))
        res = list(reports.group_items(records, ["project"], 0))
        ic(res)
        self.assertEqual(len(res), 5)

        label, group = res[0]
        self.assertEqual(label, ("<no project>",))
        self.assertEqual(group.count, 10)
        self.assertEqual(group.group_days, 0)
        self.assertEqual(group.duration, 301)

        label, group = res[1]
        self.assertEqual(label, ("project1",))
        self.assertEqual(group.count, 2)
        self.assertEqual(group.group_days, 0)
        self.assertEqual(group.duration, 53)

        label, group = res[2]
        self.assertEqual(label, ("project2",))
        self.assertEqual(group.count, 4)
        self.assertEqual(group.group_days, 0)
        self.assertEqual(group.duration, 194)

        label, group = res[3]
        self.assertEqual(label, ("tlogger",))
        self.assertEqual(group.count, 5)
        self.assertEqual(group.group_days, 0)
        self.assertEqual(group.duration, 138)

        label, group = res[4]
        self.assertEqual(label, ("<total>",))
        self.assertEqual(group.count, 21)
        self.assertEqual(group.group_days, 0)
        self.assertEqual(group.duration, 686)

    def test_week_project(self) -> None:
        records = list(record.from_string(Context(), _T1_CONTENT.split("\n")))
        res = sorted(reports.group_items(records, ["week", "project"], 0))
        ic(res)
        self.assertEqual(len(res), 15)

        label, group = res[0]
        self.assertEqual(label, ("2019-06-17",))
        self.assertEqual(group.count, 13)
        self.assertEqual(group.group_days, 7)
        self.assertEqual(group.duration, 451)

        label, group = res[1]
        self.assertEqual(label, ("2019-06-17", "<no project>"))
        self.assertEqual(group.count, 7)
        self.assertEqual(group.duration, 301)

        label, group = res[2]
        self.assertEqual(label, ("2019-06-17", "project1"))
        self.assertEqual(group.count, 1)
        self.assertEqual(group.duration, 22)

        label, group = res[3]
        self.assertEqual(label, ("2019-06-17", "project2"))
        self.assertEqual(group.count, 2)
        self.assertEqual(group.duration, 92)

        label, group = res[4]
        self.assertEqual(label, ("2019-06-17", "tlogger"))
        self.assertEqual(group.count, 3)
        self.assertEqual(group.duration, 36)

        label, group = res[5]
        self.assertEqual(label, ("2019-07-08",))
        self.assertEqual(group.count, 3)
        self.assertEqual(group.duration, 62)

        label, group = res[6]
        self.assertEqual(label, ("2019-07-08", "<no project>"))
        self.assertEqual(group.count, 1)
        self.assertEqual(group.duration, 0)

        label, group = res[7]
        self.assertEqual(label, ("2019-07-08", "project2"))
        self.assertEqual(group.count, 1)
        self.assertEqual(group.duration, 41)
