#! /usr/bin/env python
# vim:fenc=utf-8
#
# Copyright © 2020 Karol Będkowski <Karol Będkowski@kntbk>
#
# Distributed under terms of the GPLv3 license.

"""Common errors."""


class LoadError(RuntimeError):
    pass


class NoDataError(RuntimeError):
    pass
