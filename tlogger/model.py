#! /usr/bin/env python3
# vim:fenc=utf-8
#
# Copyright © 2022 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
"""
Model definition
"""
from __future__ import annotations

import datetime
from dataclasses import dataclass
from enum import Enum


class InvalidRecord(RuntimeError):
    def __init__(self, message: str, record: str):
        super().__init__(self, message)
        self.record = record

    def with_context(self, filename: str, lineno: int) -> InvalidRecord:
        return InvalidRecord(
            f"{filename}:{lineno}:{self.args[1]}", self.record
        )

    def __str__(self) -> str:
        return f"{self.args[1]} in `{self.record}`"


class RecordType(Enum):
    """Record type enum."""

    RECORD = "record"
    NOTE = "note"
    MARKER = "marker"
    COUNTER = "counter"
    OTHER = "other"


@dataclass(slots=True)
class Project:
    name: str
    type: RecordType = RecordType.RECORD


# pylint: disable=too-many-instance-attributes
@dataclass(slots=True)
class Context:
    # timestamp for previous not note/counter
    prev_marker_day: datetime.date | None = None
    prev_marker_end: datetime.datetime | None = None

    # timestamp for previous note/counter (if has time)
    prev_other_rec_day: datetime.date | None = None
    prev_other_rec_end: datetime.datetime | None = None

    prev_ts: datetime.datetime | None = None
    prev_ts_day: datetime.date | None = None
    prev_line: str = ""

    validation_enabled: bool = True
    fast_load: bool = False

    def get_prev_end(self, day: datetime.date) -> datetime.datetime | None:
        if self.prev_marker_day == day:
            return self.prev_marker_end

        if self.prev_other_rec_day == day:
            return self.prev_other_rec_end

        return None
