#!/usr/bin/python3
# vim:fenc=utf-8
#
# Copyright © 2018-2020 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.
"""Task logger."""

from __future__ import annotations

import datetime
import io
import itertools
import logging
import os
import re
import shutil
import sys
import time
import typing as ty
from contextlib import suppress
from pathlib import Path

import click
import click.shell_completion

from . import VERSION, config, errors, formatters as fmt, log, parse, reports
from .config import ConfKey, PagerMode
from .model import RecordType
from .record import Records
from .timelog import TimeLog, prepare_records_from_str

__all__ = ["cli"]

_LOG = logging.getLogger("cli")

TimeStamp = ty.Optional[datetime.datetime]


def display(lines: ty.Iterable[str], pager: PagerMode) -> None:
    """Display content of items in screen optionally using pager."""
    if pager == PagerMode.ALWAYS:
        click.echo_via_pager(line + "\n" for line in lines)
    elif pager == PagerMode.NEVER:
        for line in lines:
            click.echo(line)
    else:
        height = shutil.get_terminal_size()[1]
        buf: list[str] = []
        for idx, line in enumerate(lines):
            buf.append(line)
            if idx > height:
                # to many - use pager
                click.echo_via_pager(
                    line + "\n" for line in itertools.chain(buf, lines)
                )
                return

        for line in buf:
            click.echo(line)


def display_records(records: Records) -> None:
    for rec in records:
        click.secho(rec.raw, fg="white")


def convert_date_for_range(
    ctx: click.Context,  # pylint: disable=unused-argument
    param: click.Parameter,
    value: ty.Any,
) -> datetime.date | None:
    """Validate & convert user entered date to `datetime.date`."""
    if not value:
        return None

    with suppress(ValueError):
        off = int(value)
        return datetime.date.today() + datetime.timedelta(days=off)

    dates = parse.parse_date_to_range(value)
    if not dates:
        raise click.BadParameter(
            "invalid date; required yyyy-mm-dd, yyyy-mm or yyyy format or +/- "
            "number of days from today"
        )

    if param.name == "begin":
        return dates[0]

    return dates[1]


def convert_date_param(
    ctx: click.Context,  # pylint: disable=unused-argument
    param: click.Parameter,  # pylint: disable=unused-argument
    value: ty.Any,
) -> ty.Any:
    if not value:
        return None

    try:
        return parse.parse_date(value)
    except ValueError as err:
        raise click.BadParameter(err.args[0]) from err


def convert_datetime_param(
    ctx: click.Context,  # pylint: disable=unused-argument
    param: click.Parameter,  # pylint: disable=unused-argument
    value: ty.Any,
) -> ty.Any:
    if not value:
        return None

    try:
        return parse.parse_ts(value)
    except ValueError as err:
        raise click.BadParameter(err.args[0]) from err


def convert_duration_param(
    ctx: click.Context,  # pylint: disable=unused-argument
    param: click.Parameter,  # pylint: disable=unused-argument
    value: ty.Any,
) -> int | None:
    if not value:
        return None

    try:
        return parse.parse_duration(value)
    except ValueError as err:
        raise click.BadParameter(err.args[0]) from err


def countdown(duration: int, notification_command: str) -> None:
    """Simple pomodoro timer"""
    while duration > 0:
        click.secho("\nWorking...\n", fg="green")
        end_time = time.monotonic() + duration * 60
        try:
            while True:
                elapsed = end_time - time.monotonic()
                if elapsed <= 0:
                    break

                minutes = int(elapsed // 60)
                seconds = int(elapsed % 60)
                click.echo(f"\rElapsed: {minutes:2d}:{seconds:02d} ", nl=False)
                time.sleep(1)
        except KeyboardInterrupt:
            click.secho("\rAborted!            \a\n", fg="yellow")
        else:
            if notification_command:
                _LOG.debug("call command `%s`", notification_command)
                os.system(notification_command)

            click.secho("\rDONE!            \a\n", fg="yellow")

        if not click.confirm("Start again?"):
            return

        duration = click.prompt(
            "Duration (minutes): ", default=str(duration), type=int
        )


def last_msg_menu(tlog: TimeLog, *rec_type: RecordType) -> str | None:
    last_msg = None
    num_records = 10
    while True:
        if not last_msg:
            last_msg = list(tlog.get_last_records_msg(num_records, *rec_type))

        if not last_msg:
            return None

        click.echo("Last records:")
        for idx, msg in enumerate(last_msg, 1):
            click.echo(f"  {idx:2d}. {msg}")

        click.secho(
            """
Enter number to reuse last message, `number:` or `number: comment` to use
previous project. Pres `.` to load more messages. Or enter whole new message.
""",
            fg="cyan",
        )

        selected = click.prompt(
            "Message", default="", show_default=False
        ).strip()
        if not selected:
            return None

        if selected == ".":
            last_msg = None
            num_records += 10
            continue

        if mtch := re.match(r"^(?P<num>\d+):(?P<msg>\s+\S.+)?", selected):
            # selected number: comment; use project
            int_idx = int(mtch.group("num"))
            if 1 <= int_idx <= len(last_msg):
                project = last_msg[int_idx - 1].partition(": ")[0]
                msg = mtch.group("msg") or click.prompt(
                    f"Message (project '{project}')", default=""
                )
                if msg := msg.strip():
                    return project + ": " + msg

        elif re.match(r"^\d+$", selected):
            int_idx = int(selected)
            if 1 <= int_idx <= len(last_msg):
                return last_msg[int_idx - 1]
        else:
            return str(selected)

        click.secho("\nInvalid answer...\n", fg="bright_red")

    return None


def _add_autocomplete(
    ctx: click.Context,
    args: click.Parameter,
    incomplete: str,
    rec_type: RecordType = RecordType.RECORD,
) -> list[click.shell_completion.CompletionItem]:
    # pylint: disable=unused-argument
    """Autocomplete add arguments."""
    conf = config.read_config(None)
    if not conf.existing_file(ConfKey.MAIN_LOG_FILE):
        return []

    tlog = TimeLog(conf)
    return [
        click.shell_completion.CompletionItem(rec)
        for rec in tlog.get_last_records_msg(10, rec_type)
        if incomplete in rec
    ]


def _exit_with_error(msg: str, exit_code: int = 1) -> None:
    click.secho(msg, err=True, fg="bright_red")
    ctx = click.get_current_context()
    ctx.exit(exit_code)


def _validate_period(
    ctx: click.Context,  # pylint: disable=unused-argument
    param: click.Parameter,  # pylint: disable=unused-argument
    value: ty.Any,
) -> ty.Any:
    try:
        parse.period_to_dates(value)
        return value
    except ValueError as err:
        raise click.BadParameter("invalid period") from err


# -----------------------------------------------------------
# cli configuration
# -----------------------------------------------------------


_type_options = click.Choice(["all", "counter", "note", "record"])
_format_options = click.Choice(["text", "csv"])

CONTEXT_SETTINGS = {
    "help_option_names": ["-h", "--help"],
    "show_default": False,
}


class AliasedGroup(click.Group):
    def get_command(
        self, ctx: click.Context, cmd_name: str
    ) -> click.Command | None:
        if cmd := click.Group.get_command(self, ctx, cmd_name):
            return cmd

        matches = [
            x for x in self.list_commands(ctx) if x.startswith(cmd_name)
        ]
        if not matches:
            return None

        if len(matches) == 1:
            return click.Group.get_command(self, ctx, matches[0])

        ctx.fail("Too many matches: " + ", ".join(sorted(matches)))
        return None


@click.group(context_settings=CONTEXT_SETTINGS, cls=AliasedGroup)
@click.option(
    "-c",
    "--conf",
    "conf",
    help="Configuration file",
    default=Path(click.get_app_dir("tlogger3"), "config.ini"),
    envvar="TLOGGER_CONF",
    type=click.Path(dir_okay=False),
)
@click.option(
    "-f",
    "--file",
    "fname",
    help="Main log file name",
    envvar="TLOGGER_FILE",
    type=click.Path(dir_okay=False, writable=True),
)
@click.option(
    "--duration-format",
    help="Determine how to format duration",
    type=click.Choice(["human", "time", "minutes"]),
)
@click.option(
    "--pager",
    help="Use pager for output",
    type=click.Choice([val.value for val in PagerMode]),
)
@click.option("-v", "--verbose", "verbose", help="Verbose mode", count=True)
@click.version_option(version=VERSION, prog_name="tlogger")
@click.pass_context
def cli(ctx: click.Context, conf: str | None, **kv: ty.Any) -> None:
    log.setup_logging(kv["verbose"])
    try:
        cfg = config.read_config(
            conf,
            {
                ConfKey.MAIN_LOG_FILE: kv["fname"],
                ConfKey.FORMAT_DURATION: kv["duration_format"],
                ConfKey.MAIN_PAGER: kv["pager"],
                ConfKey.RT_VERBOSE: kv["verbose"] > 0,
            },
        )
        config.check_parent_dir(
            cfg[ConfKey.MAIN_LOG_FILE], create=True, must_exists=True
        )
        tlog = TimeLog(cfg)
        ctx.obj = ctx.with_resource(tlog)
    except io.BlockingIOError:
        ctx.fail("file is locked by another process")
    except RuntimeError as err:
        click.echo(f"[E] {err}")


# --------------------------------------------------------------------------
# adding & editing
# --------------------------------------------------------------------------

_ADD_OPT_TIME = click.option(
    "-T ",
    "--add-time",
    help="Add current time to record",
    is_flag=True,
    default=False,
)
_ADD_OPT_INPUT = click.option(
    "-i",
    "--input",
    "finput",
    help="Add records from file or stdin ('-')",
    type=click.File(),
)
_ADD_OPT_TS = click.option(
    "--ts",
    "timestamp",
    help="set date or date/time instead of current date/time",
    callback=convert_datetime_param,
)


@cli.command("add")
@_ADD_OPT_INPUT
@_ADD_OPT_TS
@click.argument("message", nargs=-1, shell_complete=_add_autocomplete)
@click.pass_obj
def cmd_add(
    tlog: TimeLog,
    message: str | None,
    finput: ty.TextIO | None = None,
    timestamp: TimeStamp = None,
) -> None:
    """
    Add entry to log.

    If input `FILENAME` is given, content is loaded from this file; use `-` for
    read  stdin.
    If `message` and `input` is not provided - show last records menu and allow
    to select on enter message.
    """
    if finput:
        message = finput.read()
    elif message:
        message = " ".join(message).strip()
    else:
        message = last_msg_menu(tlog, RecordType.RECORD)

    if not message:
        _exit_with_error("[E] missing message")

    assert message

    dts = timestamp or datetime.datetime.now()
    messages = prepare_records_from_str(message, dts)
    res, duration, num_records = tlog.append(messages)
    click.echo(f"added {num_records} records: ")
    display_records(res)
    if duration:
        dur_fmtr = fmt.get_duration_formatter(
            tlog.cfg[ConfKey.FORMAT_DURATION]
        )
        click.echo("duration: " + dur_fmtr(sec=duration))


def _note_autocomplete(
    ctx: click.Context,
    args: click.Parameter,
    incomplete: str,
) -> list[click.shell_completion.CompletionItem]:
    return _add_autocomplete(ctx, args, incomplete, RecordType.NOTE)


@cli.command("note")
@_ADD_OPT_INPUT
@_ADD_OPT_TIME
@_ADD_OPT_TS
@click.argument("message", nargs=-1, shell_complete=_note_autocomplete)
@click.pass_obj
def cmd_note(
    tlog: TimeLog,
    message: str | None,
    finput: ty.TextIO | None = None,
    add_time: bool = False,
    timestamp: TimeStamp = None,
) -> None:
    """
    Add note to log.
    """
    if finput:
        msg = finput.read()
    elif message:
        msg = " ".join(message)
    else:
        msg = click.prompt("Message")

    if not msg:
        _exit_with_error("[E] missing message")

    now = timestamp or datetime.datetime.now()
    dts = now if add_time else now.date()
    messages = prepare_records_from_str(msg, dts, "#")
    res, _, num_records = tlog.append(messages)
    click.echo(f"added {num_records} records: ")
    display_records(res)


def _counter_autocomplete(
    ctx: click.Context,
    args: click.Parameter,
    incomplete: str,
) -> list[click.shell_completion.CompletionItem]:
    return _add_autocomplete(ctx, args, incomplete, RecordType.COUNTER)


@cli.command("counter")
@_ADD_OPT_INPUT
@_ADD_OPT_TIME
@_ADD_OPT_TS
@click.argument("message", nargs=-1, shell_complete=_counter_autocomplete)
@click.pass_obj
def cmd_counter(
    tlog: TimeLog,
    message: str | None,
    finput: ty.TextIO | None = None,
    add_time: bool = False,
    timestamp: TimeStamp = None,
) -> None:
    """
    Add counter-like record to log.
    """
    if finput:
        message = finput.read()
    elif message:
        message = " ".join(message)
    else:
        message = last_msg_menu(tlog, RecordType.COUNTER)

    if not message:
        click.secho("[E] missing message", err=True, fg="bright_red")
        return

    if (
        not finput
        and "\n" not in message
        and ": +" not in message
        and ": -" not in message
        and ": =" not in message
    ):
        num = click.prompt("number to add?", default="+1")
        if not num:
            _exit_with_error("[E] missing required number")

        if num[0] not in ("-", "+"):
            num = "+" + num

        message = message.rstrip(" :") + ": " + num

    assert message

    now = timestamp or datetime.datetime.now()
    dts = now if add_time else now.date()
    msg = prepare_records_from_str(message, dts)
    res, _, num_records = tlog.append(msg)
    click.echo(f"added {num_records} records: ")
    display_records(res)


def _marker_autocomplete(
    ctx: click.Context,
    args: click.Parameter,
    incomplete: str,
) -> list[click.shell_completion.CompletionItem]:
    return _add_autocomplete(ctx, args, incomplete, RecordType.MARKER)


@cli.command("start")
@_ADD_OPT_TS
@click.argument("message", nargs=-1, shell_complete=_marker_autocomplete)
@click.pass_obj
def cmd_start(
    tlog: TimeLog,
    message: str | None,
    timestamp: TimeStamp = None,
) -> None:
    """
    Log "start**" record.
    """
    if message:
        message = " ".join(message) + " **"
    else:
        message = "start **"

    now = timestamp or datetime.datetime.now()
    msg = prepare_records_from_str(message, now)
    res, _, num_records = tlog.append(msg)
    click.echo(f"added {num_records} records: ")
    display_records(res)


@cli.command("end")
@_ADD_OPT_INPUT
@click.argument("duration", nargs=1, callback=convert_duration_param)
@click.argument("message", nargs=-1, shell_complete=_add_autocomplete)
@click.pass_obj
def cmd_end(
    tlog: TimeLog,
    duration: int,
    message: str | None = None,
    finput: ty.TextIO | None = None,
) -> None:
    """
    Log ended task with given duration.

    Add 'start' record with time calculated according to duration.
    Then add given message or load data form `input` file or ask user for
    new or previous message.
    """
    if finput:
        message = finput.read()
    elif message:
        message = " ".join(message).strip()
    else:
        message = last_msg_menu(tlog, RecordType.RECORD)

    if not message:
        _exit_with_error("[E] missing required number")

    assert message

    start = datetime.datetime.now() - datetime.timedelta(minutes=duration)
    if last_rec := tlog.get_last_record()[1]:
        if last_rec.end and start < last_rec.end:
            start = last_rec.end
            click.secho(
                "Warning: start date adjusted to end previous entry "
                f"({start})",
                fg="yellow",
            )

    click.echo("added:")
    res, _, _ = tlog.append(prepare_records_from_str("start **", start))
    display_records(res)
    res, _, _ = tlog.append(
        prepare_records_from_str(message, datetime.datetime.now())
    )
    display_records(res)


@cli.command("pomodoro")
@click.option(
    "-d",
    "--duration",
    help="Duration as number of minutes or hh:mm (default 15)",
    default="15",
    callback=convert_duration_param,
)
@click.option(
    "--start/--no-start", help="Add start record on beginning", default=True
)
@click.option(
    "-C",
    "--notification-command",
    envvar="TLOGGER_NOTIF_CMD",
    help="Command to call on time run up",
    type=str,
)
@click.pass_obj
@click.pass_context
def cmd_pomodoro(
    ctx: click.Context,
    tlog: TimeLog,
    duration: int,
    start: bool,
    notification_command: str,
) -> None:
    """
    Pomodoro timer.

    On beginning add 'start' entry (if `--start` flag is used).
    Countdown given `duration` and ask for stop/continue. On stop - ask user
    for record to add.
    """

    tlog.cfg.update_path(ConfKey.NOTIFY_CMD, notification_command)

    if start:
        res, _, num_records = tlog.append(
            prepare_records_from_str("start **", datetime.datetime.now())
        )
        click.echo(f"added {num_records} records: ")
        display_records(res)

    countdown(duration, tlog.cfg[ConfKey.NOTIFY_CMD])
    ctx.invoke(cmd_add)


_TMPL_HEAD = """# Lines started with `#` and blank lines are ignored.
# `*:` or `**:` on line beginning is replace with current date / date time.
# Lines without date are prefixed with date. Lines with time (ie. `12:34:`) on
# beginning are complete with current date.
"""
_TMPL_FILE_HEAD = """
# TEMPLATE START"""


@cli.command("day", context_settings={"ignore_unknown_options": True})
@click.option(
    "-t",
    "--template",
    help="Content template",
    type=click.Path(dir_okay=False, exists=True, readable=True),
    envvar="TLOGGER_DAILY_TMPL",
)
@click.argument(
    "date",
    nargs=1,
    default="today",
    callback=convert_date_param,
)
@click.pass_obj
def cmd_day(
    tlog: TimeLog,
    date: datetime.date,
    template: str | None = None,
) -> None:
    """
    Edit current on given day records. If template is available also use items
    from it.

    Optional argument is date to edit (in format %Y-%m-%m or today, yesterday,
    or -<days> ago)",
    """
    tlog.cfg.update_path(ConfKey.MAIN_TEMPLATE_FILE, template)
    tmpl_file = tlog.cfg.existing_file(ConfKey.MAIN_TEMPLATE_FILE)
    tmpl = [_TMPL_HEAD]
    try:
        tmpl.extend(rec.raw for rec in tlog.get_day_records(date))
    except (errors.LoadError, OSError) as err:
        _exit_with_error(f"[E] Error load data: {err}\n")

    now, today = _preprare_today_now(date)

    if tmpl_file:
        template = Path(tmpl_file).read_text(encoding="UTF-8")
        template = template.rstrip().replace("%D", today).replace("%T", now)
        tmpl.extend((_TMPL_FILE_HEAD, template))

    err_msg = ""
    while True:
        content = click.edit(err_msg + "\n".join(tmpl), require_save=True)
        if not content:
            return

        lines = [
            line
            for line in prepare_records_from_str(content, date)
            if not line.startswith("# Error: ")
        ]

        try:
            tlog.update_day(date, lines, replace=True)
            return
        except (errors.LoadError, OSError) as err:
            err_msg = f"# Error: {err}\n"
            tmpl = lines


def _preprare_today_now(date: datetime.date | None) -> tuple[str, str]:
    if date and date != datetime.date.today():
        now = date.strftime(fmt.TS_FORMAT)
        today = date.strftime(fmt.TSD_FORMAT)
    else:
        now = datetime.datetime.now().strftime(fmt.TS_FORMAT)
        today = datetime.date.today().strftime(fmt.TSD_FORMAT)

    return now, today


# --------------------------------------------------------------------------
# REPORTS
# --------------------------------------------------------------------------


_RAP_OPT_GROUP = click.option(
    "-g",
    "--group",
    "groups",
    help="Group records by",
    multiple=True,
    type=click.Choice(list(reports.REPORT_GROUPS)),
)
_RAP_OPT_PERIOD = click.option(
    "-p",
    "--period",
    "period",
    help="Select record by dates (yyyy, yyyy-mm, yyyy-mm-dd) or "
    + " by names time period; i.e.: "
    + ", ".join(reports.PERIODS),
    callback=_validate_period,
)
_RAP_OPT_SUMMARY = click.option(
    "--summary/--no-summary", help="Show summary information", default=True
)
_RAP_OPT_DETAILS = click.option(
    "--details/--no-details",
    help="Show detailed information about records",
    default=False,
)
_RAP_OPT_TYPE = click.option(
    "--type", help="Records to show", multiple=True, type=_type_options
)
_RAP_OPT_PROJECT = click.option("--project", "projects", multiple=True)
_RAP_OPT_TAG = click.option("--tag", "tags", multiple=True)
_RAP_OPT_BEGIN = click.option(
    "-b",
    "--begin",
    help="Show records not earlier than given date",
    callback=convert_date_for_range,
)
_RAP_OPT_END = click.option(
    "-e",
    "--end",
    help="Show records not older than given date",
    callback=convert_date_for_range,
)
_RAP_OPT_DEPTH = click.option(
    "--max-depth",
    help="Maximum depth of groups",
    type=click.IntRange(min=0),
    default=0,
)
_RAP_OPT_AVG = click.option(
    "--average-in-period",
    help="Average gauge items in whole period instead only occurred days",
    is_flag=True,
)
_RAP_OPT_FORMAT = click.option(
    "--format", help="Output format", type=_format_options
)
_RAP_OUT_WIDTH = click.option(
    "--output-width",
    help="overwrite output width (default: use terminal width or 80)",
    type=int,
)
_RAP_OPT_FILTERS = click.argument("filters", nargs=-1)


@cli.command("report")
@_RAP_OPT_GROUP
@_RAP_OPT_DEPTH
@_RAP_OPT_PERIOD
@_RAP_OPT_SUMMARY
@_RAP_OPT_DETAILS
@_RAP_OPT_TYPE
@_RAP_OPT_PROJECT
@_RAP_OPT_TAG
@_RAP_OPT_BEGIN
@_RAP_OPT_END
@_RAP_OPT_FORMAT
@_RAP_OPT_AVG
@_RAP_OPT_FILTERS
@_RAP_OUT_WIDTH
@click.pass_obj
def cmd_report(tlog: TimeLog, **opts: ty.Any) -> None:
    """Show records report.

    Report can show records filtered by date (period and/or begin/end dates),
    projects, tags and in-message words.

    Records can be grouped by project, dates etc.
    """

    if sys.stdout.isatty():
        opts["max-width"] = (
            opts.get("output_width") or shutil.get_terminal_size()[0]
        )
    else:
        opts["max-width"] = opts.get("output_width") or 80

    opts["duration_format"] = tlog.cfg[ConfKey.FORMAT_DURATION]
    try:
        display(reports.run_report(tlog, opts), tlog.cfg[ConfKey.MAIN_PAGER])
    except (errors.LoadError, errors.NoDataError) as err:
        _exit_with_error(str(err))


@cli.command("today")
@_RAP_OPT_GROUP
@_RAP_OPT_DEPTH
@_RAP_OPT_SUMMARY
@_RAP_OPT_DETAILS
@_RAP_OPT_TYPE
@_RAP_OPT_PROJECT
@_RAP_OPT_TAG
@_RAP_OPT_BEGIN
@_RAP_OPT_END
@_RAP_OPT_FORMAT
@_RAP_OPT_FILTERS
@_RAP_OUT_WIDTH
@click.pass_context
def cmd_report_today(ctx: click.Context, **opts: ty.Any) -> None:
    """
    Show report for today records.
    """
    ctx.invoke(cmd_report, period="today", **opts)


@cli.command("yesterday")
@_RAP_OPT_GROUP
@_RAP_OPT_DEPTH
@_RAP_OPT_SUMMARY
@_RAP_OPT_DETAILS
@_RAP_OPT_TYPE
@_RAP_OPT_PROJECT
@_RAP_OPT_TAG
@_RAP_OPT_BEGIN
@_RAP_OPT_END
@_RAP_OPT_FORMAT
@_RAP_OPT_FILTERS
@_RAP_OUT_WIDTH
@click.pass_context
def cmd_report_yesterday(ctx: click.Context, **opts: ty.Any) -> None:
    """
    Show report for yesterday records.
    """
    ctx.invoke(cmd_report, period="yesterday", **opts)


@cli.command("status")
@click.pass_obj
def cmd_status(tlog: TimeLog) -> None:
    """
    Display information about today records.
    """
    try:
        records = list(tlog.get_day_records())
    except (errors.LoadError, OSError) as err:
        _exit_with_error(f"[E] Error load data: {err}\n")
    if not records:
        click.echo("# no data")
        return

    opts: dict[str, ty.Any] = {}
    if sys.stdout.isatty():
        opts["max-width"] = (
            opts.get("output_width") or shutil.get_terminal_size()[0]
        )
    else:
        opts["max-width"] = opts.get("output_width") or 80

    opts["duration_format"] = tlog.cfg[ConfKey.FORMAT_DURATION]

    report = reports.format_details(records, fmt.tabularize, opts)
    duration = int(
        (
            datetime.datetime.now() - max(rec.end for rec in records)
        ).total_seconds()
    )
    dur_fmtr = fmt.get_duration_formatter(tlog.cfg[ConfKey.FORMAT_DURATION])
    report = itertools.chain(
        report, ("\nFrom last: " + dur_fmtr(sec=duration),)
    )
    display(report, tlog.cfg[ConfKey.MAIN_PAGER])


@cli.command("show")
@click.option(
    "--all",
    "load_all",
    help="Show all data (instead of current year)",
    is_flag=True,
)
@click.argument(
    "what", nargs=1, type=click.Choice(["projects", "tags", "last-record"])
)
@click.pass_obj
def cmd_show(tlog: TimeLog, what: str, load_all: bool) -> None:
    """
    Show informations about records (projects, tags).

    By default records are loaded only from current year; use `--all` option
    to load all available files.
    """
    ts_begin = None
    if not load_all:
        ts_begin = datetime.date.today().replace(day=1, month=1)

    res = None
    try:
        match what:
            case "projects":
                res = list(tlog.get_projects(ts_begin))
            case "tags":
                res = list(tlog.get_tags(ts_begin))
            case "last-record":
                last_rec, oldest_rec = tlog.get_last_record()
                if last_rec and oldest_rec:
                    res = [
                        f"Last record:   {last_rec.raw}",
                        f"Oldest record: {oldest_rec.raw}",
                    ]
    except (errors.LoadError, errors.NoDataError) as err:
        _exit_with_error(str(err))

    if res:
        res.sort()
        display(res, tlog.cfg[ConfKey.MAIN_PAGER])
    else:
        click.echo("# no data")


@cli.command("print")
@_RAP_OPT_PERIOD
@_RAP_OPT_TYPE
@_RAP_OPT_PROJECT
@_RAP_OPT_TAG
@_RAP_OPT_BEGIN
@_RAP_OPT_END
@_RAP_OPT_FILTERS
@_RAP_OUT_WIDTH
@click.option("--format", help="format records", is_flag=True, default=False)
@click.pass_obj
def cmd_print(tlog: TimeLog, **opts: ty.Any) -> None:
    """Print records with or without formatting.

    Report can show records filtered by date (period and/or begin/end dates),
    projects, tags and in-message words.
    """
    try:
        display(reports.run_print(tlog, opts), tlog.cfg[ConfKey.MAIN_PAGER])
    except (errors.LoadError, errors.NoDataError) as err:
        _exit_with_error(str(err))


# --------------------------------------------------------------------------
# misc commands
# --------------------------------------------------------------------------


@cli.command("edit")
@click.pass_obj
def cmd_edit(tlog: TimeLog) -> None:
    """
    Edit log file with external editor.
    """
    click.edit(filename=tlog.fname)


@cli.command("archive")
@click.pass_obj
def cmd_archive(tlog: TimeLog) -> None:
    """
    Move old (previous years) records into separate files.
    """
    try:
        display(tlog.archive(), tlog.cfg[ConfKey.MAIN_PAGER])
    except (errors.LoadError, errors.NoDataError) as err:
        _exit_with_error(str(err))


@cli.command("undo")
@click.pass_obj
def cmd_undo(tlog: TimeLog) -> None:
    """
    Remove last record from logfile.
    """
    try:
        res = tlog.undo()
        click.echo(f"Removed: {res}")
    except (errors.LoadError, errors.NoDataError) as err:
        _exit_with_error(str(err))


@cli.command("print-config")
@click.pass_obj
def cmd_print_config(tlog: TimeLog) -> None:
    """
    Display current program configuration.
    """
    cfg = tlog.cfg
    for key, val in cfg.items():
        click.echo(f"{key}={val}")


@cli.command("check")
@click.pass_obj
def cmd_check(tlog: TimeLog) -> None:
    """
    Check file for errors.
    """
    for err in tlog.check():
        click.echo(err)


@cli.command("import")
@_ADD_OPT_INPUT
@click.pass_obj
def cmd_import(tlog: TimeLog, finput: ty.TextIO) -> None:
    """
    Import raw data into log file.

    """
    message = finput.read()
    if not message:
        _exit_with_error("[E] missing data to import")

    res, duration, num_records = tlog.append(message)
    click.echo(f"added {num_records} records: ")
    display_records(res)
    if duration:
        dur_fmtr = fmt.get_duration_formatter(
            tlog.cfg[ConfKey.FORMAT_DURATION]
        )
        click.echo("duration: " + dur_fmtr(sec=duration))


@cli.command("import-diary")
@click.option(
    "--date",
    "date",
    callback=convert_date_param,
)
@click.argument(
    "file",
    nargs=1,
    type=click.Path(exists=True, dir_okay=False, resolve_path=True),
)
@click.pass_obj
def cmd_import_diary(
    tlog: TimeLog, file: str, date: datetime.date | None = None
) -> None:
    """
    Import data from "Diary" Android application.

    TODO: parse optional time.
    """

    if not date:
        # try guess date from path; path is default .../yyyy/mm/dd.txt
        parts = file.split(os.path.sep)
        if len(parts) > 3:
            try:
                day = int(parts[-1][:2])
                month = int(parts[-2])
                year = int(parts[-3])
                date = datetime.date(year, month, day)
            except ValueError:
                _exit_with_error(f"[E] can't guess date from path {file}")

    if not date:
        _exit_with_error("[E] missing date")

    content = Path(file).read_text(encoding="UTF-8")
    if not content:
        _exit_with_error("[E] missing data to import")

    assert date
    lines = prepare_records_from_str(content, date)
    try:
        tlog.update_day(date, lines, replace=False)
    except (errors.LoadError, OSError) as err:
        _exit_with_error(f"[E] Error load data: {err}\n")


@cli.command("migrate")
@click.argument(
    "output",
    nargs=1,
    type=click.Path(
        exists=False, dir_okay=False, file_okay=True, writable=True
    ),
)
@click.pass_obj
def cmd_migrate(tlog: TimeLog, output: str) -> None:
    """
    Migrate data to other storage
    """
    num_records = tlog.migrate(output)
    click.echo(f"Migration finished; migrated {num_records} records")


if __name__ == "__main__":
    with suppress(KeyboardInterrupt, EOFError):
        cli(None, None)
