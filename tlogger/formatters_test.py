# Copyright © 2020 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.
# pylint: disable=protected-access

"""Tests for formatters module."""

import datetime
import unittest

from . import formatters as fmt


class TestFormatters(unittest.TestCase):
    def test_datetime(self) -> None:
        self.assertEqual(fmt.format_datetime(None), "")
        self.assertEqual(
            fmt.format_datetime(
                datetime.datetime(2020, 3, 10, 22, 58, 0), True
            ),
            "2020-03-10 22:58",
        )
        self.assertEqual(
            fmt.format_datetime(
                datetime.datetime(2020, 3, 10, 22, 58, 0), False
            ),
            "22:58",
        )

    def test_duration(self) -> None:
        self.assertEqual(fmt.format_duration(), "")
        self.assertEqual(fmt.format_duration(0), "")
        self.assertEqual(fmt.format_duration(0, 0), "")
        self.assertEqual(fmt.format_duration(0, 1), "1m")
        self.assertEqual(fmt.format_duration(0, 60), "1m")
        self.assertEqual(fmt.format_duration(0, 70), "2m")
        self.assertEqual(fmt.format_duration(0, 90), "2m")
        self.assertEqual(fmt.format_duration(0, 120), "2m")
        self.assertEqual(fmt.format_duration(1), "1m")
        self.assertEqual(fmt.format_duration(2), "2m")
        self.assertEqual(fmt.format_duration(30), "30m")
        self.assertEqual(fmt.format_duration(30, 2), "31m")
        self.assertEqual(fmt.format_duration(30, 60), "31m")
        self.assertEqual(fmt.format_duration(30, 70), "32m")
        self.assertEqual(fmt.format_duration(60), "1h")
        self.assertEqual(fmt.format_duration(61), "1h 1m")
        self.assertEqual(fmt.format_duration(120), "2h")
        self.assertEqual(fmt.format_duration(130), "2h 10m")
        self.assertEqual(fmt.format_duration(130, 0), "2h 10m")
        self.assertEqual(fmt.format_duration(130, 5), "2h 11m")
        self.assertEqual(fmt.format_duration(130, 61), "2h 12m")


class TestTabularize(unittest.TestCase):
    def test_wo_headers(self):
        rows = [[1, 3, "4", 5], [65, 7234, 8], [11, 2, 34444, 4, 555, "6"]]
        formatted = list(fmt.tabularize(rows))
        self.assertEqual(len(formatted), 3)
        self.assertEqual(formatted[0], "1     3       4        5")
        self.assertEqual(formatted[1], "65    7234    8")
        self.assertEqual(formatted[2], "11    2       34444    4    555    6")

    def test_o_headers(self):
        rows = [[1, 3, "4", 5], [65, 7234, 8], [11, 2, 34444, 4, 555, "6"]]
        header = ["aaa", "bbbb", "c", "d", "ee"]
        formatted = list(fmt.tabularize(rows, header=header))
        self.assertEqual(len(formatted), 5)
        self.assertEqual(formatted[0], "aaa    bbbb    c        d    ee")
        self.assertEqual(formatted[1], "--------------------------------")
        self.assertEqual(formatted[2], "1      3       4        5")
        self.assertEqual(formatted[3], "65     7234    8")
        self.assertEqual(formatted[4], "11     2       34444    4    555    6")
