#! /usr/bin/env python
# vim:fenc=utf-8
#
# Copyright © 2020 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.
# pylint: disable=protected-access

"""Tests for parse module."""
import datetime
import unittest

from freezegun import freeze_time

from . import parse


class TestParse(unittest.TestCase):
    @freeze_time("2020-03-05")
    def test_period_to_date_range_last1(self) -> None:
        self.assertEqual(
            parse._period_to_date_range_last("day"),
            (datetime.date(2020, 3, 4), datetime.date(2020, 3, 5)),
        )
        self.assertEqual(
            parse._period_to_date_range_last("week"),
            (datetime.date(2020, 2, 27), datetime.date(2020, 3, 5)),
        )
        self.assertEqual(
            parse._period_to_date_range_last("month"),
            (datetime.date(2020, 2, 5), datetime.date(2020, 3, 5)),
        )
        self.assertEqual(
            parse._period_to_date_range_last("year"),
            (datetime.date(2019, 3, 5), datetime.date(2020, 3, 5)),
        )

        with self.assertRaises(ValueError):
            parse._period_to_date_range_last("adsad")

        # with self.assertRaises(ValueError):
        #     parse._period_to_date_range_last(None)

    @freeze_time("2021-01-02")
    def test_period_to_date_range_last2(self) -> None:
        self.assertEqual(
            parse._period_to_date_range_last("day"),
            (datetime.date(2021, 1, 1), datetime.date(2021, 1, 2)),
        )
        self.assertEqual(
            parse._period_to_date_range_last("week"),
            (datetime.date(2020, 12, 26), datetime.date(2021, 1, 2)),
        )
        self.assertEqual(
            parse._period_to_date_range_last("month"),
            (datetime.date(2020, 12, 2), datetime.date(2021, 1, 2)),
        )
        self.assertEqual(
            parse._period_to_date_range_last("year"),
            (datetime.date(2020, 1, 2), datetime.date(2021, 1, 2)),
        )

    @freeze_time("2020-03-05")
    def test_period_to_date_range_prev1(self) -> None:
        self.assertEqual(
            parse._period_to_date_range_previous("day"),
            (datetime.date(2020, 3, 4), datetime.date(2020, 3, 4)),
        )
        self.assertEqual(
            parse._period_to_date_range_previous("week"),
            (datetime.date(2020, 2, 24), datetime.date(2020, 3, 1)),
        )
        self.assertEqual(
            parse._period_to_date_range_previous("month"),
            (datetime.date(2020, 2, 1), datetime.date(2020, 2, 29)),
        )
        self.assertEqual(
            parse._period_to_date_range_previous("year"),
            (datetime.date(2019, 1, 1), datetime.date(2019, 12, 31)),
        )

        with self.assertRaises(ValueError):
            parse._period_to_date_range_previous("daada")

        # with self.assertRaises(ValueError):
        #     parse._period_to_date_range_previous(None)

    @freeze_time("2021-01-02")
    def test_period_to_date_range_prev2(self) -> None:
        self.assertEqual(
            parse._period_to_date_range_previous("day"),
            (datetime.date(2021, 1, 1), datetime.date(2021, 1, 1)),
        )
        self.assertEqual(
            parse._period_to_date_range_previous("week"),
            (datetime.date(2020, 12, 21), datetime.date(2020, 12, 27)),
        )
        self.assertEqual(
            parse._period_to_date_range_previous("month"),
            (datetime.date(2020, 12, 1), datetime.date(2020, 12, 31)),
        )
        self.assertEqual(
            parse._period_to_date_range_previous("year"),
            (datetime.date(2020, 1, 1), datetime.date(2020, 12, 31)),
        )

    @freeze_time("2020-03-05")
    def test_period_to_date_range_this(self) -> None:
        self.assertEqual(
            parse._period_to_date_range_this("day"),
            (datetime.date(2020, 3, 5), datetime.date(2020, 3, 5)),
        )
        self.assertEqual(
            parse._period_to_date_range_this("week"),
            (datetime.date(2020, 3, 2), datetime.date(2020, 3, 8)),
        )
        self.assertEqual(
            parse._period_to_date_range_this("month"),
            (datetime.date(2020, 3, 1), datetime.date(2020, 3, 31)),
        )
        self.assertEqual(
            parse._period_to_date_range_this("year"),
            (datetime.date(2020, 1, 1), datetime.date(2020, 12, 31)),
        )

        with self.assertRaises(ValueError):
            parse._period_to_date_range_this("daada")

        # with self.assertRaises(ValueError):
        #     parse._period_to_date_range_this(None)

    @freeze_time("2020-03-05")
    def test_period_to_date_range(self) -> None:
        with self.assertRaises(ValueError):
            parse.period_to_date_range("daada-dada")

        with self.assertRaises(ValueError):
            parse.period_to_date_range("")

        with self.assertRaises(ValueError):
            parse.period_to_date_range("day")

        self.assertEqual(parse.period_to_date_range("all"), None)

        self.assertEqual(
            parse.period_to_date_range("last-day"),
            (datetime.date(2020, 3, 4), datetime.date(2020, 3, 5)),
        )

        self.assertEqual(
            parse.period_to_date_range("previous-week"),
            (datetime.date(2020, 2, 24), datetime.date(2020, 3, 1)),
        )

        self.assertEqual(
            parse.period_to_date_range("this-month"),
            (datetime.date(2020, 3, 1), datetime.date(2020, 3, 31)),
        )

        self.assertEqual(
            parse.period_to_date_range("-3"),
            (datetime.date(2020, 3, 2), datetime.date(2020, 3, 2)),
        )

        self.assertEqual(
            parse.period_to_date_range("+11"),
            (datetime.date(2020, 3, 16), datetime.date(2020, 3, 16)),
        )


class TestPeriodToDates(unittest.TestCase):
    @freeze_time("2020-03-05")
    def test_period_to_dates(self) -> None:
        self.assertEqual(
            parse.period_to_dates(None, None, None),
            (datetime.date(2020, 3, 5), datetime.date(2020, 3, 5)),
        )
        self.assertEqual(
            parse.period_to_dates("today"),
            (datetime.date(2020, 3, 5), datetime.date(2020, 3, 5)),
        )
        self.assertEqual(
            parse.period_to_dates("this-day"),
            (datetime.date(2020, 3, 5), datetime.date(2020, 3, 5)),
        )
        self.assertEqual(
            parse.period_to_dates("yesterday"),
            (datetime.date(2020, 3, 4), datetime.date(2020, 3, 4)),
        )
        self.assertEqual(
            parse.period_to_dates("last-year"),
            (datetime.date(2019, 3, 5), datetime.date(2020, 3, 5)),
        )
        self.assertEqual(
            parse.period_to_dates("previous-year"),
            (datetime.date(2019, 1, 1), datetime.date(2019, 12, 31)),
        )
        # shortcut
        self.assertEqual(
            parse.period_to_dates("previous-month"),
            (datetime.date(2020, 2, 1), datetime.date(2020, 2, 29)),
        )

    @freeze_time("2020-03-05")
    def test_period_to_dates_begin(self) -> None:
        self.assertEqual(
            parse.period_to_dates(None, datetime.date(2020, 2, 7), None),
            (datetime.date(2020, 2, 7), datetime.date(2020, 3, 5)),
        )
        self.assertEqual(
            parse.period_to_dates(
                "yesterday", datetime.date(2020, 2, 7), None
            ),
            (datetime.date(2020, 3, 4), datetime.date(2020, 3, 4)),
        )
        self.assertEqual(
            parse.period_to_dates(
                "previous-year", datetime.date(2020, 2, 7), None
            ),
            (datetime.date(2020, 2, 7), datetime.date(2019, 12, 31)),
        )

    @freeze_time("2020-03-05")
    def test_period_to_dates_end(self) -> None:
        self.assertEqual(
            parse.period_to_dates(None, None, datetime.date(2020, 2, 7)),
            (datetime.date(2020, 2, 7), datetime.date(2020, 2, 7)),
        )
        self.assertEqual(
            parse.period_to_dates(
                "last-month", None, datetime.date(2020, 2, 14)
            ),
            (datetime.date(2020, 2, 5), datetime.date(2020, 2, 14)),
        )
        self.assertEqual(
            parse.period_to_dates(
                "previous-year", None, datetime.date(2020, 2, 7)
            ),
            (datetime.date(2019, 1, 1), datetime.date(2019, 12, 31)),
        )

    @freeze_time("2020-03-05")
    def test_period_to_dates_begin_end(self) -> None:
        self.assertEqual(
            parse.period_to_dates(
                None,
                datetime.date(2020, 1, 2),
                datetime.date(2020, 2, 7),
            ),
            (datetime.date(2020, 1, 2), datetime.date(2020, 2, 7)),
        )
        self.assertEqual(
            parse.period_to_dates(
                "previous-month",
                datetime.date(2020, 2, 10),
                datetime.date(2020, 2, 14),
            ),
            (datetime.date(2020, 2, 10), datetime.date(2020, 2, 14)),
        )
        self.assertEqual(
            parse.period_to_dates(
                "previous-month",
                datetime.date(2020, 1, 10),
                datetime.date(2020, 2, 14),
            ),
            (datetime.date(2020, 2, 1), datetime.date(2020, 2, 14)),
        )
        self.assertEqual(
            parse.period_to_dates(
                "previous-month",
                datetime.date(2020, 2, 5),
                datetime.date(2020, 3, 14),
            ),
            (datetime.date(2020, 2, 5), datetime.date(2020, 2, 29)),
        )
        self.assertEqual(
            parse.period_to_dates(
                "all",
                datetime.date(2020, 2, 5),
                datetime.date(2020, 3, 14),
            ),
            (datetime.date(2020, 2, 5), datetime.date(2020, 3, 14)),
        )


class TestDuration(unittest.TestCase):
    def test_parse_duration(self) -> None:
        self.assertEqual(parse.parse_duration(1234), 1234)
        self.assertEqual(parse.parse_duration("12:34:56"), 12 * 60 + 34)
        self.assertEqual(parse.parse_duration("02:32:11"), 2 * 60 + 32)
        self.assertEqual(parse.parse_duration("12:34"), 12 * 60 + 34)
        self.assertEqual(parse.parse_duration("1:33"), 1 * 60 + 33)

        with self.assertRaises(ValueError):
            parse.parse_duration(None)

        # with self.assertRaises(ValueError):
        #     parse.parse_duration(-1)

        # with self.assertRaises(ValueError):
        #     parse.parse_duration(-0)

        with self.assertRaises(ValueError):
            parse.parse_duration("lad;a")

        with self.assertRaises(ValueError):
            parse.parse_duration("25:22")


class TestParseRangesFromDate(unittest.TestCase):
    def test_invalid(self) -> None:
        self.assertEqual(parse.parse_date_to_range("2021  "), None)
        self.assertEqual(parse.parse_date_to_range("2022-33"), None)
        self.assertEqual(parse.parse_date_to_range("2022-12-31 2"), None)
        self.assertEqual(parse.parse_date_to_range(" 2022-12-12"), None)

    def test_year(self) -> None:
        self.assertEqual(
            parse.parse_date_to_range("2021"),
            (
                datetime.date(2021, 1, 1),
                datetime.date(2021, 12, 31),
            ),
        )

    def test_month(self) -> None:
        self.assertEqual(
            parse.parse_date_to_range("2021-01"),
            (
                datetime.date(2021, 1, 1),
                datetime.date(2021, 1, 31),
            ),
        )
        self.assertEqual(
            parse.parse_date_to_range("2022-02"),
            (
                datetime.date(2022, 2, 1),
                datetime.date(2022, 2, 28),
            ),
        )

    def test_day(self) -> None:
        self.assertEqual(
            parse.parse_date_to_range("2021-01-03"),
            (
                datetime.date(2021, 1, 3),
                datetime.date(2021, 1, 3),
            ),
        )
        self.assertEqual(
            parse.parse_date_to_range("2020-02-28"),
            (
                datetime.date(2020, 2, 28),
                datetime.date(2020, 2, 28),
            ),
        )

    @freeze_time("2020-06-13")
    def test_offset(self) -> None:
        self.assertEqual(
            parse.parse_date_to_range("-10"),
            (
                datetime.date(2020, 6, 3),
                datetime.date(2020, 6, 3),
            ),
        )

        self.assertEqual(
            parse.parse_date_to_range("+10"),
            (
                datetime.date(2020, 6, 23),
                datetime.date(2020, 6, 23),
            ),
        )
