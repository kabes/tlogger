#! /usr/bin/env python
# vim:fenc=utf-8
#
# Copyright © 2020  Karol Będkowski
#
# Distributed under terms of the GPLv3 license.

"""Record definition and parsing."""
from __future__ import annotations

import datetime
import operator
import re
import typing as ty
from decimal import Decimal, InvalidOperation

from . import formatters as fmt, parse
from .errors import LoadError
from .model import Context, InvalidRecord, RecordType


class Record:  # pylint: disable=too-many-instance-attributes
    """One record (line) in time log."""

    __slots__ = (
        "kind",
        "_raw",
        "begin",
        "end",
        "project",
        "message",
        "cnt",
        "day",
        "daily_record",
        "note",
    )

    def __init__(
        self,
        timestamp: datetime.datetime,
        raw: str | None = None,
        note: list[str] | None = None,
        kind: RecordType = RecordType.RECORD,
    ):
        if raw and note:
            raw += "\n" + "\n".join(note)

        self.kind: RecordType = kind
        # record raw content
        self._raw: str | None = raw
        # begin timestamp from previous entry (computed)
        self.begin: datetime.datetime | None = None
        # end timestamp from file as datetime; if record is day-type, set
        # to midnight
        self.end: datetime.datetime = timestamp
        # project name
        self.project: str = ""
        # content without date, project and cnt
        self.message: str = ""
        # counter
        self.cnt: Decimal | None = None

        # day extracted for datetime
        self.day = timestamp.date()

        self.daily_record: bool = False
        self.note: list[str] | None = note

    @staticmethod
    def new_other(raw: str) -> Record:
        """Other is dummy record - only raw part is significant."""
        return Record(datetime.datetime.min, raw, kind=RecordType.OTHER)

    @property
    def raw(self) -> str:
        return self._raw or self.build_raw()

    @property
    def is_counter(self) -> bool:
        # for now it is not necessary to check self.cnt is not None
        return self.kind == RecordType.COUNTER

    @property
    def is_note(self) -> bool:
        return (self.kind == RecordType.NOTE) or (
            (self.kind == RecordType.COUNTER) and bool(self.message)
        )

    def is_type(self, rtype: RecordType) -> bool:
        """
        Check is record match given type.
        I.e. note can have counter and counter can have message, so in some
        situation they may be other types.
        """
        return (
            rtype == self.kind
            # if cnt is set type of record is always counter
            # or (rtype == RecordType.COUNTER and self.cnt is not None)
            or (
                rtype == RecordType.NOTE
                and self.kind == RecordType.COUNTER
                and bool(self.message)
            )
        )

    def __repr__(self) -> str:
        # items = ", ".join(
        #            f"{k}={v!r}" for k, v in sorted(self.__dict__.items())
        #       )
        items = ", ".join(
            f"{k}={getattr(self, k)!r}" for k in sorted(self.__slots__)
        )
        return f"<{self.__class__.__name__}: {items}>"

    def __str__(self) -> str:
        if self.kind == RecordType.OTHER:
            return self.raw

        res = [
            repr(self.raw),
            self.kind.value,
            (f"{self.begin} - " if self.begin else "") + str(self.end),
            f"dur={self.duration}",
            f"cnt={self.cnt}",
            f"proj={self.project}",
        ]
        return ", ".join(map(str, res))

    def human_timestamp(self) -> str:
        if self.kind == RecordType.OTHER:
            return ""

        if self.daily_record:
            return self.end.strftime(fmt.TSD_FORMAT)

        return self.end.strftime(fmt.TS_FORMAT)

    @property
    def duration(self) -> int:
        """Task duration in minutes"""
        if self.begin:
            secs = (self.end - self.begin).total_seconds()
            return int(secs // 60) + 1

        return 0

    @property
    def full_message(self) -> str:
        """
        Return project name and message for reports.
        """
        if self.project:
            return f"{self.project}: {self.clean_message}"

        return self.clean_message

    @property
    def tags(self) -> ty.Set[str]:
        return _parse_tags(self.raw)

    @property
    def contexts(self) -> ty.Set[str]:
        return _parse_contexts(self.raw)

    @property
    def clean_message(self) -> str:
        msg = self.message
        if not msg:
            return ""

        msg = msg.lstrip(" #")

        if (idx := msg.rfind(" #")) > 1:
            return msg[:idx]

        return msg

    def build_raw(self) -> str:
        """
        Assemble `raw` format from record.
        """
        parts = [self.human_timestamp() + ":"]

        if self.project:
            parts.append(self.project + ":")

        if (cnt := self.cnt) is not None:
            if cnt.is_signed():
                parts.append(str(cnt))
            else:
                parts.append(f"+{cnt}")

        if self.message:
            parts.append(self.message)

        return " ".join(parts) + ("\n".join(self.note) if self.note else "")

    def update_from_ctx(self, ctx: Context) -> None:
        if self.kind == RecordType.OTHER:
            return

        timestamp = self.end

        if self.kind == RecordType.RECORD:
            assert self.day
            self.begin = ctx.get_prev_end(self.day)

        if self.kind in (RecordType.RECORD, RecordType.MARKER):
            ctx.prev_marker_end = timestamp
            ctx.prev_marker_day = self.day
        elif not self.daily_record:
            ctx.prev_other_rec_end = timestamp
            ctx.prev_other_rec_day = self.day

        ctx.prev_ts = timestamp
        ctx.prev_ts_day = self.day
        ctx.prev_line = self.raw


def parse_tlog_line(
    ctx: Context, line: str, note: list[str] | None = None
) -> Record:
    """Create record from text.

    Format:

    Record:
        yyyy-mm-dd hh:mm[:ss]: [project: ]message[ # comment]
    Marker (non-counted action):
        yyyy-mm-dd hh:mm[:ss]: [project: ]message **[ # comment]
    Note:
        yyyy-mm-dd[ hh:mm[:ss]]: [project: ]# message
        yyyy-mm-dd[ hh:mm[:ss]]: [project: ]message *[ # comment]
    Counter:
        yyyy-mm-dd[ hh:mm[:ss]]: [project: ]+<number>[ message][ # comment]
        yyyy-mm-dd[ hh:mm[:ss]]: [project: ]-<number>[ message][ # comment]
        yyyy-mm-dd[ hh:mm[:ss]]: [project: ]=<number>[ message][ # comment]

    Message can contain:
        Tags: +<name>
        Context: @<name>

    Project in form:
        <name>:[sub_project:]*

    """
    line = line.strip()
    tstamp, _, content = line.partition(": ")
    if not content:
        raise InvalidRecord("invalid record", line)

    content = content.strip(" \t")
    if not content:
        raise InvalidRecord("invalid record: no content", line)

    timestamp, only_day = _parse_check_date(ctx, tstamp, line)
    record = Record(timestamp, line, note)
    record.daily_record = only_day

    if ctx.fast_load:
        return record

    content, record.project = _parse_project(content)
    if only_day:
        # only_day records can be only counters or note
        content, is_counter = _parse_counter(content, line, record)
        record.message = content
        if not is_counter:
            record.kind = RecordType.NOTE
    else:
        # marker
        if content.endswith("**"):
            record.message = content
            record.kind = RecordType.MARKER
        # note
        elif content.startswith("#") or content.endswith("*"):
            record.message = content
            content = content.lstrip(" #")
            record.kind = RecordType.NOTE
        else:
            content, _ = _parse_counter(content, line, record)
            record.message = content

    record.update_from_ctx(ctx)
    return record


def _parse_tags(content: str) -> ty.Set[str]:
    re_tag = re.compile(r"\B\+\D[\w:_]+\b", re.UNICODE)
    return set(re_tag.findall(content))


def _parse_contexts(content: str) -> ty.Set[str]:
    re_context = re.compile(r"\B@\D[\w_]+\b", re.UNICODE)
    return set(re_context.findall(content))


_RE_PROJECT = re.compile(r"^([^W\d_][\w-]*:)+$")


def _parse_project(content: str) -> tuple[str, str]:
    """
    Split `content` into message and project.

    Returns:
        (message, project)
    """
    project, _, rest = content.partition(": ")

    if rest and _RE_PROJECT.match(project + ":"):
        return rest, project

    return content, ""


def _parse_counter(
    content: str, line: str, record: Record
) -> tuple[str, bool]:
    cnt, _, rest = content.partition(" ")

    if not re.match(r"^[+=-](\d*\.)?\d+$", cnt):
        return content, False

    if not record.project:
        raise InvalidRecord("counters require project", line)

    if cnt[0] == "=":
        cnt = cnt[1:]

    try:
        record.cnt = Decimal(cnt)
    except (ValueError, InvalidOperation) as err:
        raise InvalidRecord(
            f"invalid counter, can't parse '{cnt}'", line
        ) from err

    record.kind = RecordType.COUNTER
    return rest, True


def _parse_check_date(
    ctx: Context, strtimestamp: str, line: str
) -> tuple[datetime.datetime, bool]:
    try:
        tstamp, ts_with_time = parse.parse_datetime(strtimestamp)
    except ValueError as err:
        raise InvalidRecord(f"invalid date '{strtimestamp}'", line) from err

    if not ctx.validation_enabled:
        return tstamp, not ts_with_time

    failed = False
    if ts_with_time:
        failed = ctx.prev_ts is not None and tstamp < ctx.prev_ts
    else:
        failed = (
            ctx.prev_ts_day is not None and tstamp.date() < ctx.prev_ts_day
        )

    if failed:
        raise InvalidRecord(
            f"date out of order (prev: `{ctx.prev_line}`)", line
        )

    return tstamp, not ts_with_time


Records = ty.Iterable[Record]
InputLine = tuple[str, int, str]
ChunkBundle = tuple[str, int, list[str]]


def from_string(
    ctx: Context,
    lines: ty.Iterable[str],
    sort: bool = False,
    filename: str | None = None,
    preserve: bool = False,
) -> Records:
    """
    Parse input data into Records or str

    Args:
        ctx: cotnext
        lines: input lines to parse
        sort: sort chunks (default false)
        filename: input file name (optional)
        preserve: preserve comments (default false)
    """
    assert (
        not sort and not preserve
    ) or sort != preserve, "either sort or preserve may be set"

    chunks = load_chunks(lines, preserve, filename)
    if sort:
        chunks = sorted(chunks, key=operator.itemgetter(2))

    for fname, lineno, chunk in chunks:
        if preserve:
            header = chunk[0]
            if not header or header[0] == "#":
                yield Record.new_other(header)
                continue

        try:
            yield parse_tlog_line(ctx, chunk[0], chunk[1:])
        except InvalidRecord as err:
            raise LoadError(
                f"Invalid record in `{fname}`:{lineno}: {err}"
            ) from err


def load_chunks(
    lines: ty.Iterable[str],
    preserve: bool = False,
    filename: str | None = None,
) -> ty.Iterable[ChunkBundle]:
    chunk: list[str] = []
    chunk_fname: str = ""
    chunk_lineno: int = 0
    filename = filename or ""

    for lineno, line in enumerate(lines, 1):
        line = line.rstrip()

        if not line or line[0] == "#":
            if preserve:
                if chunk:
                    chunk.append(line)
                else:
                    yield filename, lineno, [line]

            continue

        if line[0] in (" ", "\x09"):
            if chunk:
                chunk.append(line)

            continue

        if chunk:
            yield chunk_fname, chunk_lineno, chunk

        chunk = [line]
        chunk_fname = filename
        chunk_lineno = lineno

    if chunk:
        yield chunk_fname, chunk_lineno, chunk
