# Copyright © 2020 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.

"""Reporting functions."""
from __future__ import annotations

import calendar
import collections
import datetime
import itertools
import operator
import typing as ty
from dataclasses import dataclass
from decimal import Decimal

from . import filters, formatters as fmt, timelog as tl
from .config import ConfKey
from .model import RecordType
from .record import Record


def get_record_day(rec: Record) -> str:
    return rec.end.strftime("%Y-%m-%d")


def get_record_week(rec: Record) -> str:
    dte = rec.end - datetime.timedelta(days=rec.end.weekday())
    return dte.strftime("%Y-%m-%d")


def get_record_month(rec: Record) -> str:
    return rec.end.strftime("%Y-%m")


def get_record_year(rec: Record) -> str:
    return str(rec.end.year)


def get_project(rec: Record) -> ty.Iterable[str]:
    return rec.project or "<no project>"


def get_project_rec(rec: Record) -> ty.Iterable[str]:
    return rec.project.split(":") if rec.project else ("<no project>",)


ValueGetter = ty.Callable[[Record], ty.Union[str, ty.Iterable[str]]]


_GROUPBY_FUNCS: dict[str, ValueGetter] = {
    "day": get_record_day,
    "week": get_record_week,
    "month": get_record_month,
    "year": get_record_year,
    "project": get_project,
    "project-rec": get_project_rec,
    "message": operator.attrgetter("message"),
    "full-message": operator.attrgetter("full_message"),
    "default": operator.attrgetter("full_message"),
}

REPORT_GROUPS = set(_GROUPBY_FUNCS.keys())
PERIODS = {
    "all",
    "last-day",
    "last-week",
    "last-month",
    "last-year",
    "yesterday",
    "today",
    "this-week",
    "this-month",
    "this-year",
    "previous-day",
    "previous-week",
    "previous-month",
    "previous-year",
}

TyRecords = ty.Iterable[Record]

# -----------------------------------------------------------
# reports
# -----------------------------------------------------------


@dataclass(slots=True)
class Group:
    count: int = 0
    duration: int = 0
    _sum_cnt: Decimal = Decimal(0)
    min_ts: datetime.datetime = datetime.datetime.max
    max_ts: datetime.datetime = datetime.datetime.min
    group_days: int = 0

    def add(self, item: Record, period: str | None = None) -> None:
        if not item.end:
            return

        self.count += 1
        self.duration += item.duration
        self._sum_cnt += item.cnt or Decimal(0)
        self.min_ts = min(self.min_ts, item.end)
        self.max_ts = max(self.max_ts, item.end)
        if period and not self.group_days:
            self.set_group_daterange(period)

    @property
    def sum_cnt(self) -> Decimal:
        if self._sum_cnt != self._sum_cnt.to_integral():
            return self._sum_cnt.quantize(Decimal(".01"))

        return self._sum_cnt

    def avg(self) -> Decimal:
        if self.count:
            avg = self._sum_cnt / self.count
            if avg != avg.to_integral():
                return avg.quantize(Decimal(".01"))

            return avg

        return Decimal(0)

    def avg_in_period(self) -> Decimal:
        if self.count and self.group_days:
            avg = self._sum_cnt / self.group_days
            if avg != avg.to_integral():
                return avg.quantize(Decimal(".01"))

        return Decimal(0)

    def set_group_daterange(self, period: str) -> None:
        match period:
            case "week":
                self.group_days = 7
            case "month":
                dtb = self.min_ts
                self.group_days = calendar.monthrange(dtb.year, dtb.month)[1]
            case "year":
                dtb = self.min_ts
                self.group_days = 366 if calendar.isleap(dtb.year) else 365
            case "day":
                self.group_days = 1

    def __add__(self, other: Group) -> Group:
        ngr = Group()
        ngr.count = self.count + other.count
        ngr.duration = self.duration + other.duration
        ngr._sum_cnt = self._sum_cnt + other._sum_cnt
        ngr.min_ts = min(ngr.min_ts, other.min_ts)
        ngr.max_ts = max(ngr.max_ts, other.max_ts)
        return ngr


def _build_group_period_list(groups: ty.Collection[str]) -> ty.Iterable[str]:
    """Separate date-like groups from all `groups`, duplicate
    previous date group when other is given.

    ie ["month", "project", "year"] -> ["month", "month", "year"]
    """
    cur_period = ""
    for grp in groups:
        if grp in ("week", "month", "year", "day"):
            cur_period = grp

        yield cur_period


def get_item_path(
    item: Record, getters: ty.Collection[ValueGetter]
) -> ty.Iterable[tuple[int, str]]:
    """
    Get group values for record.

    Return
        iterable of <group index>, <group value>
    """
    for idx, getter in enumerate(getters):
        keys = getter(item)
        if not keys:
            continue

        if isinstance(keys, str):
            yield idx, keys
        else:
            for key in keys:
                yield idx, key


def group_items(
    items: TyRecords, groups: ty.Collection[str], max_depth: int = 0
) -> ty.Iterable[tuple[tuple[str, ...], Group]]:
    getters = tuple(_GROUPBY_FUNCS[group] for group in groups)
    group_periods = tuple(_build_group_period_list(groups))
    grps: dict[tuple[str, ...], Group] = collections.defaultdict(Group)

    for item in items:
        path = list(get_item_path(item, getters))
        if max_depth:
            path = path[:max_depth]

        while path:
            path_key: tuple[str, ...] = tuple(val[1] for val in path)
            period_name = group_periods[path[-1][0]]
            grps[path_key].add(item, period_name)
            path.pop()

    if group_names := sorted(grps.keys()):
        total = Group()
        for gname in group_names:
            yield gname, grps[gname]
            total = total + grps[gname]

        # total
        yield ("<total>",), total


def _build_filter(opts: dict[str, ty.Any]) -> filters.Filter:
    # it there is not selected period and neither begin nor end is given
    # set period to this-year.
    if not opts.get("period"):
        if not opts.get("begin") and not opts.get("end"):
            opts["period"] = "this-year"

    return filters.Filter(
        date_begin=opts.get("begin"),
        date_end=opts.get("end"),
        types=opts.get("type"),
        period=opts.get("period"),
        words=opts.get("filters"),
        projects=opts.get("projects"),
        tags=opts.get("tags"),
    )


def _run_report_summary(
    items: TyRecords,
    groups: ty.Collection[str],
    opts: dict[str, ty.Any],
    formatter: fmt.Formatter,
) -> ty.Iterable[str]:
    sitems = (item for item in items if item.kind == RecordType.RECORD)

    dur_fmtr = fmt.get_duration_formatter(opts["duration_format"])
    max_depth = opts.get("max_depth", 0)
    gitems = tuple(
        (gname, dur_fmtr(ginfo.duration), ginfo.count)
        for gname, ginfo in group_items(sitems, groups, max_depth)
    )
    if gitems:
        yield from _generate_report_split(
            gitems,
            ("Project", "Duration", "Count"),
            formatter,
            (fmt.ColAlign.LEFT, fmt.ColAlign.RIGHT, fmt.ColAlign.RIGHT),
        )
        yield ""


def _run_report_counter(
    items: TyRecords,
    groups: list[str],
    opts: dict[str, ty.Any],
    formatter: fmt.Formatter,
) -> ty.Iterable[str]:
    max_depth = opts.get("max_depth", 0)
    sitems = group_items(
        (item for item in items if item.is_counter), groups, max_depth
    )

    if opts.get("average_in_period"):
        pitems = (
            (gname, ginfo.count, ginfo.sum_cnt, ginfo.avg_in_period())
            for gname, ginfo in sitems
            if gname
        )
    else:
        pitems = (
            (gname, ginfo.count, ginfo.sum_cnt, ginfo.avg())
            for gname, ginfo in sitems
            if gname
        )

    gitems = tuple(
        (gname, count, f"{sum_cnt}", f"{avg}")
        for gname, count, sum_cnt, avg in pitems
    )
    if gitems:
        yield from _generate_report_split(
            gitems,
            ("Counter", "Count", "Sum", "Avg"),
            formatter,
            (
                fmt.ColAlign.LEFT,
                fmt.ColAlign.RIGHT,
                fmt.ColAlign.RIGHT,
                fmt.ColAlign.RIGHT,
            ),
        )
        yield ""


def _run_report_note(
    items: TyRecords, opts: dict[str, ty.Any], formatter: fmt.Formatter
) -> ty.Iterable[str]:
    nitems = [item for item in items if item.is_note]
    if nitems:
        fitems = (
            ((item.human_timestamp(),), item.project, item.clean_message)
            for item in nitems
        )

        ropts = {
            "max-width": opts["max-width"],
            "col-wrap": -1,
        }

        yield from _generate_report_split(
            fitems,
            ("Note", "Category", "Note"),
            formatter,
            opts=ropts,
        )
        yield ""


def _run_report_details(
    items: TyRecords, opts: dict[str, ty.Any], formatter: fmt.Formatter
) -> ty.Iterable[str]:
    yield "Details"
    yield "======="
    if items:
        yield from format_details(items, formatter, opts)
    else:
        yield "<no items>"

    yield ""


def run_report(tlog: tl.TimeLog, opts: dict[str, ty.Any]) -> ty.Iterable[str]:
    items = list(tlog.load(fltr=_build_filter(opts)))
    entries_type = opts.get("type")
    entries_type = set(entries_type or ("counter", "record", "note"))
    formatter = fmt.get_report_formatter(opts.get("format"))

    if items:
        dstart = items[0].end.strftime("%Y-%m-%d")
        dend = items[-1].end.strftime("%Y-%m-%d")
        yield f"Report for items since {dstart} to {dend}\n"

    if opts.get("details"):
        yield from _run_report_details(items, opts, formatter)

    if opts.get("summary"):
        groups = opts.get("groups") or ["default"]
        yield from _run_report_summary(items, groups, opts, formatter)

    if "counter" in entries_type:
        groups = opts.get("groups") or ["project"]
        yield from _run_report_counter(items, groups, opts, formatter)

    if "note" in entries_type:
        yield from _run_report_note(items, opts, formatter)


def format_details(
    records: TyRecords,
    formatter: fmt.Formatter,
    opts: dict[str, ty.Any],
) -> ty.Iterable[str]:
    dur_fmtr = fmt.get_duration_formatter(opts["duration_format"])

    formatted_records = (
        (
            fmt.format_date(rec.begin or rec.end)
            if rec.daily_record
            else fmt.format_datetime(rec.begin or rec.end, True),
            fmt.format_datetime(rec.end) if rec.duration else "",
            rec.project,
            rec.clean_message,
            dur_fmtr(rec.duration) if rec.kind == RecordType.RECORD else "",
            _format_decimal(rec.cnt) if rec.is_counter else "",
        )
        for rec in records
    )

    ropts = {
        "max-width": opts["max-width"],
        "col-wrap": 3,
    }

    yield from formatter(
        formatted_records,
        ("Begin", "End", "Project", "Message", "Duration", "Count"),
        (
            fmt.ColAlign.LEFT,
            fmt.ColAlign.LEFT,
            fmt.ColAlign.LEFT,
            fmt.ColAlign.LEFT,
            fmt.ColAlign.RIGHT,
            fmt.ColAlign.RIGHT,
        ),
        ropts,
    )


ReportRow = ty.Sequence[ty.Any]


def _generate_report_split(
    items: ty.Iterable[ReportRow],
    cols: ty.Sequence[str],
    formatter: fmt.Formatter,
    align: ty.Sequence[fmt.ColAlign] | None = None,
    opts: dict[str, ty.Any] | None = None,
) -> ty.Iterable[str]:
    """Split first column (that should contain sequence of group values)
    into separate columns and format using `formatter`.

    `align` set columns alignment; first item is apply to all group values.

    ie:
    items: [
        [[group1, group2], val1, val2],
        [[group3], val3]
    ]
    cols: ["g1", "v1", v2"]
    align: [RIGHT, LEFT, CENTER]

    into:
    items [
        [group1, group2 , val1, val2],
        [group3, ""     , val3]
    ]
    cols: ["g1", "", "v1", v2"]
    align: [RIGHT, RIGHT, LEFT, CENTER]
    """
    check_items, items = itertools.tee(items)
    max_groups = max(len(item[0]) for item in check_items)

    def split_item(item: ReportRow) -> ty.Iterator[ty.Any]:
        groups, *rest = item
        yield from groups
        yield from ("" for _ in range(len(groups), max_groups))
        yield from rest

    first_col, *cols_rest = cols
    cols = [first_col] + [""] * (max_groups - 1) + cols_rest

    if align:
        first, *rest = align
        align = [first] * max_groups + rest

    new_items = (list(split_item(item)) for item in items)
    return formatter(new_items, cols, align, opts)


def run_print(tlog: tl.TimeLog, opts: dict[str, ty.Any]) -> ty.Iterable[str]:
    records = tlog.load(fltr=_build_filter(opts))

    if opts.get("format"):
        prev_day = None
        for rec in records:
            if prev_day and prev_day != rec.day:
                yield ""

            prev_day = rec.day
            yield rec.raw

    elif tlog.cfg[ConfKey.RT_VERBOSE]:
        yield from map(str, records)

    else:
        for rec in records:
            yield rec.raw


def _format_decimal(value: Decimal | None) -> str:
    if value is None:
        return ""

    if value != value.to_integral():
        return str(value.quantize(Decimal(".01")))

    return str(value)
