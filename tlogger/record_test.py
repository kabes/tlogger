# Copyright © 2020 Karol Będkowski
#
# Distributed under terms of the GPLv3 license.
# pylint: disable=protected-access

"""Tests for Record."""
import datetime
import unittest
from contextlib import suppress

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from . import record
from .model import Context, InvalidRecord


class TestParseTlogLine(unittest.TestCase):
    def test0(self) -> None:
        ctx = Context()
        with self.assertRaises(InvalidRecord):
            record.parse_tlog_line(ctx, "")

        with self.assertRaises(InvalidRecord):
            record.parse_tlog_line(ctx, "        ")

        with self.assertRaises(InvalidRecord):
            record.parse_tlog_line(ctx, "2019-01-02 03:04:   ")

        with self.assertRaises(InvalidRecord):
            record.parse_tlog_line(ctx, "2019-01-02 03:04:          ")

    def test1(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04: project1: message # comment"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.human_timestamp(), "2019-01-02 03:04")
        self.assertEqual(rec.end, datetime.datetime(2019, 1, 2, 3, 4, 0))
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "project1")
        self.assertEqual(rec.message, "message # comment")
        self.assertEqual(rec.clean_message, "message")
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.day, datetime.date(2019, 1, 2))
        self.assertEqual(rec.kind, record.RecordType.RECORD)
        self.assertEqual(rec.raw, line)

    def test2(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04: project1 message"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.human_timestamp(), "2019-01-02 03:04")
        self.assertEqual(rec.end, datetime.datetime(2019, 1, 2, 3, 4, 0))
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "")
        self.assertEqual(rec.message, "project1 message")
        self.assertEqual(rec.kind, record.RecordType.RECORD)

    def test_messsage(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04: proj2: abc #"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.message, "abc #")
        self.assertEqual(rec.clean_message, "abc")

        line = "2019-01-02 03:04: proj2: abc # qqqw"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.message, "abc # qqqw")
        self.assertEqual(rec.clean_message, "abc")

        line = "2019-01-02 03:04: proj2: # abc # qqqw"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.message, "# abc # qqqw")
        self.assertEqual(rec.clean_message, "abc")

    def test3(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04 project1 message"
        with self.assertRaises(InvalidRecord):
            record.parse_tlog_line(ctx, line)

        line = "2019-01-02 03: project1 message"
        with self.assertRaises(InvalidRecord):
            record.parse_tlog_line(ctx, line)

        line = "2019-01-02 03:03:"
        with self.assertRaises(InvalidRecord):
            record.parse_tlog_line(ctx, line)

    def test4(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04: start **"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.human_timestamp(), "2019-01-02 03:04")
        self.assertEqual(rec.end, datetime.datetime(2019, 1, 2, 3, 4, 0))
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "")
        self.assertEqual(rec.kind, record.RecordType.MARKER)

    def test5(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04: start fsf sfsfs **"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.human_timestamp(), "2019-01-02 03:04")
        self.assertEqual(rec.end, datetime.datetime(2019, 1, 2, 3, 4, 0))
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "")
        self.assertEqual(rec.kind, record.RecordType.MARKER)

    def test_projects(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04: project1: abc:dkld dkdk: ee"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.project, "project1")

        line = "2019-01-02 03:04: project1:proj2: abc:dkld dkdk: ee"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.project, "project1:proj2")

        line = "2019-01-02 03:04: p roject1:proj2: abc:dkld dkdk: ee"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.project, "")

        line = "2019-01-02 03:04: project1 :proj2: abc:dkld dkdk: ee"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.project, "")

        line = "2019-01-02 03:04: proj2: #"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.project, "proj2")
        self.assertEqual(rec.message, "#")
        self.assertEqual(rec.clean_message, "")

    def test6_comments(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04: start fsf sfsfs *"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.kind, record.RecordType.NOTE)

        line = "# 2019-01-02 03:03: sdlak"
        with self.assertRaises(InvalidRecord):
            record.parse_tlog_line(ctx, line)

    def test7_tags(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04: start fsf sfsfs +tag1 +tag2 +232 a+1 # da"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.human_timestamp(), "2019-01-02 03:04")
        self.assertEqual(rec.end, datetime.datetime(2019, 1, 2, 3, 4, 0))
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "")
        self.assertEqual(rec.tags, {"+tag1", "+tag2"})
        self.assertEqual(rec.kind, record.RecordType.RECORD)

    def test8_context(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04: start fsf @ctx2 @ctx1 +tag2 +232 a+1 # da"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.human_timestamp(), "2019-01-02 03:04")
        self.assertEqual(rec.end, datetime.datetime(2019, 1, 2, 3, 4, 0))
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "")
        self.assertEqual(rec.tags, {"+tag2"})
        self.assertEqual(rec.contexts, {"@ctx1", "@ctx2"})
        self.assertEqual(rec.kind, record.RecordType.RECORD)
        self.assertEqual(rec.raw, line)

    def test_note(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04: #start fsf sfsfs +tag1 +tag2 +232 a+1 # da"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.human_timestamp(), "2019-01-02 03:04")
        self.assertEqual(rec.end, datetime.datetime(2019, 1, 2, 3, 4, 0))
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "")
        self.assertEqual(rec.tags, {"+tag1", "+tag2"})
        self.assertEqual(rec.kind, record.RecordType.NOTE)
        self.assertIsNone(rec.cnt)

    def test_notes(self) -> None:
        ctx = Context()
        line = """2019-01-02 03:04: #start fsf sfsfs +tag1 +tag2 +232 a+1 # da
    abc d adda
    dalskdal da"""
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.human_timestamp(), "2019-01-02 03:04")
        self.assertEqual(rec.end, datetime.datetime(2019, 1, 2, 3, 4, 0))
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "")
        self.assertEqual(rec.tags, {"+tag1", "+tag2"})
        self.assertEqual(rec.kind, record.RecordType.NOTE)
        self.assertIsNone(rec.cnt)

    def test_counter(self) -> None:
        ctx = Context()
        line = (
            "2019-01-02 03:04: h:test: +1 start "
            "fsf sfsfs +tag1 +tag2 +232 a+1 # da"
        )
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.human_timestamp(), "2019-01-02 03:04")
        self.assertEqual(rec.end, datetime.datetime(2019, 1, 2, 3, 4, 0))
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "h:test")
        self.assertEqual(rec.tags, {"+tag1", "+tag2"})
        self.assertEqual(rec.kind, record.RecordType.COUNTER)
        self.assertEqual(rec.raw, line)

        line = (
            "2019-01-02 03:04: h: +2 start fsf @sfsfs +tag1 +tag2 "
            "+232 a+1 # da"
        )
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.human_timestamp(), "2019-01-02 03:04")
        self.assertEqual(rec.end, datetime.datetime(2019, 1, 2, 3, 4, 0))
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "h")
        self.assertEqual(rec.tags, {"+tag1", "+tag2"})
        self.assertEqual(rec.kind, record.RecordType.COUNTER)
        self.assertEqual(rec.contexts, {"@sfsfs"})
        self.assertEqual(rec.cnt, 2)
        self.assertEqual(rec.raw, line)

        line = "2019-01-02 03:04: ddd:ddd: +5"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "ddd:ddd")
        self.assertEqual(rec.cnt, 5)

        line = "2019-01-02 03:04: h:test: =1 abc"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.human_timestamp(), "2019-01-02 03:04")
        self.assertEqual(rec.end, datetime.datetime(2019, 1, 2, 3, 4, 0))
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.project, "h:test")
        self.assertEqual(rec.cnt, 1)
        self.assertEqual(rec.kind, record.RecordType.COUNTER)
        self.assertEqual(rec.raw, line)
        self.assertEqual(rec.message, "abc")

    def test_counter_invalid(self) -> None:
        ctx = Context()
        line = "2019-01-02 03:04: +2"
        with self.assertRaises(InvalidRecord):
            record.parse_tlog_line(ctx, line)

        line = "2019-01-02 03:04: proj: +2dkdkdk"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.kind, record.RecordType.RECORD)

        line = "2019-01-02 03:04: proj: +2-2"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.kind, record.RecordType.RECORD)


class TestParseTags(unittest.TestCase):
    def test_parse_tags1(self):
        line = "abc +abc dldld"
        self.assertEqual(record._parse_tags(line), {"+abc"})

        line = "abc +abc dldld +abc2"
        self.assertEqual(record._parse_tags(line), {"+abc", "+abc2"})

        line = "abc +abc dl+dld +abc"
        self.assertEqual(record._parse_tags(line), {"+abc"})

    def test_parse_tags_w_val(self):
        line = "abc +abc:ddd dldld"
        self.assertEqual(record._parse_tags(line), {"+abc:ddd"})

        line = "+abc:ddd dldld +abc:aaa"
        self.assertEqual(record._parse_tags(line), {"+abc:ddd", "+abc:aaa"})

        line = "+abc:ddd +dldld +abc:aaa"
        self.assertEqual(
            record._parse_tags(line), {"+abc:ddd", "+abc:aaa", "+dldld"}
        )

    def test_build_raw(self):
        ctx = Context()
        line = "2019-01-02 03:04: proj2: abc #"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.build_raw(), line)

        line = "2019-01-02 03:04: proj2: abc # qqqw"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.build_raw(), line)

        line = "2019-01-02 03:04: proj2: # abc # qqqw"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.build_raw(), line)

        line = "2019-01-02 03:04: # abc # qqqw"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.build_raw(), line)

        line = "2019-01-02: p:aa: +2 # abc # qqqw"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.build_raw(), line)

        line = "2019-01-02: p: -2 # abc # qqqw"
        rec = record.parse_tlog_line(ctx, line)
        self.assertEqual(rec.build_raw(), line)


class TestParseContext(unittest.TestCase):
    def test_parse_context1(self) -> None:
        line = "abc @abc dldld"
        self.assertEqual(record._parse_contexts(line), {"@abc"})

        line = "abc @abc dldld @abc"
        self.assertEqual(record._parse_contexts(line), {"@abc"})

        line = "abc @abc dldld @abcd"
        self.assertEqual(record._parse_contexts(line), {"@abc", "@abcd"})

        line = "abc @abc_d dldld @abcd12"
        self.assertEqual(record._parse_contexts(line), {"@abc_d", "@abcd12"})


_T2_CONTENT = ["2019-06-19 15:13: start **", "2019-06-19 19:38: learn r2"]

_T3_CONTENT = [
    "2019-06-19 15:13: start **",
    "# comments",
    "2019-06-19 19:38: learn r2",
    "",
    "2019-06-19 19:42: start **",
    " ",
]


_T5_CONTENT = [
    "2019-06-19 14:07: entry1",
    "    this is note line1",
    "   also note line 2",
    "2019-06-20: aaa: bbbb",
]


class TestLoadChunks(unittest.TestCase):
    def test_simple(self):
        chunks = list(record.load_chunks(_T2_CONTENT))
        self.assertEqual(len(chunks), 2)
        self.assertEqual(chunks[0], ("", 1, [_T2_CONTENT[0]]))
        self.assertEqual(chunks[1], ("", 2, [_T2_CONTENT[1]]))

    def test_nopreserve(self):
        chunks = list(record.load_chunks(_T3_CONTENT))
        self.assertEqual(len(chunks), 3)
        self.assertEqual(chunks[0], ("", 1, [_T3_CONTENT[0]]))
        self.assertEqual(chunks[1], ("", 3, [_T3_CONTENT[2]]))
        self.assertEqual(chunks[2], ("", 5, [_T3_CONTENT[4]]))

    def test_preserve(self):
        chunks = list(record.load_chunks(_T3_CONTENT, preserve=True))
        self.assertEqual(len(chunks), 3)
        self.assertEqual(chunks[0], ("", 1, [_T3_CONTENT[0], _T3_CONTENT[1]]))
        self.assertEqual(chunks[1], ("", 3, [_T3_CONTENT[2], _T3_CONTENT[3]]))
        self.assertEqual(chunks[2], ("", 5, [_T3_CONTENT[4], ""]))

    def test_note(self):
        chunks = list(record.load_chunks(_T5_CONTENT))
        self.assertEqual(len(chunks), 2)
        self.assertEqual(chunks[0], ("", 1, _T5_CONTENT[0:3]))
        self.assertEqual(chunks[1], ("", 4, [_T5_CONTENT[3]]))


_T1_CONTENT = """
2019-06-19 15:13: start **
#2019-06-19 15:16: learn: r
2019-06-19 19:38: learn r2
2019-06-19 19:42: start **
2019-06-19 20:04: tlogger: cleanup *
2019-06-19 20:16: learn r2

2019-06-20 13:03: start **
2019-06-20 13:26: tlogger: cleanup
2019-06-20 13:37: tlogger: cleanup
2019-06-20 14:07: start **
"""


class TestTimeLog(unittest.TestCase):
    def test_load(self):
        records = list(record.from_string(Context(), _T1_CONTENT.split("\n")))
        self.assertEqual(len(records), 9)

    def test_calc_duration(self):
        records = list(record.from_string(Context(), _T1_CONTENT.split("\n")))
        self.assertEqual(records[0].duration, 0)
        self.assertEqual(records[1].duration, 266)
        self.assertEqual(records[2].duration, 0)
        self.assertEqual(records[3].duration, 0)
        self.assertEqual(records[4].duration, 35)
        self.assertEqual(records[5].duration, 0)
        self.assertEqual(records[6].duration, 24)
        self.assertEqual(records[7].duration, 12)
        self.assertEqual(records[8].duration, 0)
