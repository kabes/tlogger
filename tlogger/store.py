#! /usr/bin/env python3
# vim:fenc=utf-8
#
# Copyright © 2022 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Abstract store
"""
from __future__ import annotations

import abc
import datetime
import itertools
import logging
import typing as ty

from . import filters, utils
from .model import Context, RecordType
from .record import Record, Records

_LOG = logging.getLogger(__name__)


class AbstractStore(metaclass=abc.ABCMeta):
    """
    Abstract Store

    """

    @abc.abstractmethod
    def open(self) -> None:
        raise NotImplementedError()

    @abc.abstractmethod
    def close(self) -> None:
        raise NotImplementedError()

    @abc.abstractmethod
    def load(
        self,
        ctx: Context,
        fltr: filters.Filter | None = None,
    ) -> Records:
        """
        Load records from files.

        Args:
            ctx: context
            fltr: optional filters

        Returns:
            Records
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def get_last_record(self) -> tuple[Record | None, Record | None]:
        """
        Get fast last record from current log in the same day.

        Returns:
            (last record in file, oldest record in file)
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def get_last_records_msg(
        self, num: int, *kind: RecordType
    ) -> ty.Iterable[str]:
        """
        Get unique last `num` records message given type.
        Read only current and previous year files (if exists).

        Args:
            num: number of records to load
            kind: list types of records to load

        Returns:
            last messages as str
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def append(self, records: Records) -> tuple[int, int]:
        """
        Appends records into store.

        Args:
            records: records to add

        Returns:
            (number of added records, optional duration from previous record)
            Where number added records exceed 10, add `...` and number of total
            records.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def list(self) -> ty.Iterable[str]:
        """
        Get content of current file (year).
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def get_day_records(self, date: datetime.date | None = None) -> Records:
        """
        Get records from current or given day.

        Args:
            data: optional date of records to fetch (default today)
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def archive(self) -> ty.Generator[str, None, None]:
        """
        Archive old records.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def undo(self) -> str:
        """
        Remove last record from file.

        Return:
            removed record.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def update_day(
        self, date: datetime.date, content: ty.Iterable[str], replace: bool
    ) -> None:
        """
        Add or replace entries from given day from content.
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def check(self) -> ty.Iterable[str]:
        """
        Check storage.
        """
        raise NotImplementedError()

    def migrate(
        self,
        store: AbstractStore,
        fltr: filters.Filter | None = None,
    ) -> int:
        ctx = Context()
        records = self.load(ctx, fltr=fltr)

        def log(cnt: int) -> None:
            _LOG.info("migrated %d records", cnt)

        records = utils.call_every_n(records, 100, log)
        num_records, _ = store.append(records)
        return num_records

    def get_projects(
        self,
        ctx: Context,
        ts_begin: datetime.date | None = None,
        ts_end: datetime.date | None = None,
    ) -> ty.Iterable[str]:
        """
        Get projects names from given date range.
        """
        fltr = filters.Filter(date_begin=ts_begin, date_end=ts_end)
        records = self.load(ctx, fltr=fltr)
        return {c.project for c in records if c.project}

    def get_tags(
        self,
        ctx: Context,
        ts_begin: datetime.date | None = None,
        ts_end: datetime.date | None = None,
    ) -> ty.Iterable[str]:
        """
        Get used tags from given date range.
        """
        fltr = filters.Filter(date_begin=ts_begin, date_end=ts_end)
        records = self.load(ctx, fltr=fltr)
        return set(itertools.chain(*(e.tags or [] for e in records)))
